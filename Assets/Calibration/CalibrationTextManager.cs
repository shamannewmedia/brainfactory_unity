﻿using UnityEngine;
using System.Collections;

public class CalibrationTextManager : MonoBehaviour {

    public GameObject panel;

    public GameObject brainShape;
    public GameObject step1, step2, step3;
    public GameObject congratulations;

    private bool showing = false;

	// Use this for initialization
	void Start () {
        ResetToZero();
    }

    private void ResetToZero()
    {
        brainShape.SetActive(false);

        step1.SetActive(false);
        step2.SetActive(false);
        step3.SetActive(false);
        congratulations.SetActive(false);

        panel.SetActive(false);

        showing = false;
    }

    public void Show(int step)
    {
        if (showing)
            return;

        panel.SetActive(true);

        switch (step)
        {
            case 0: brainShape.SetActive(true); break;
            case 1: step1.SetActive(true); break;
            case 2: step2.SetActive(true); break;
            case 3: step3.SetActive(true); break;
            case 4: congratulations.SetActive(true); break;
        }

        showing = true;
    }

    public void HideAll()
    {
        ResetToZero();
    }
}
