﻿using UnityEngine;
using System.Collections;

public class CurveVisualization : MonoBehaviour {
	public float particleInitRad = 4.0f;
	public float particleSize = 0.1f;
	public float Z_Speed = 1.2f;
	public float Radious_Speed = 1.5f;
	public int numOfEachWaveTotal = 100;
	public int numOfWaveTotal = 14;
	public float radarSpeed = 10.0f;

	float[] raw_data;
	float[] particleRad;
	float[] z;
	GameObject[] prefab;

	int numWave = 0;

	int isRestart = 0;

	bool[] isTint;

	void Start () { 
		
		raw_data = new float[numOfEachWaveTotal * numOfWaveTotal];
		prefab = new GameObject[numOfEachWaveTotal * numOfWaveTotal];
		particleRad = new float[numOfEachWaveTotal * numOfWaveTotal];
		z = new float[numOfEachWaveTotal * numOfWaveTotal];
		isTint = new bool[numOfEachWaveTotal];

		for (int i = 0 ; i < numOfWaveTotal * numOfEachWaveTotal; i++) {
			prefab [i] = (GameObject)Instantiate (Resources.Load ("particleBall"));
			prefab [i].SetActive (false);
            prefab[i].transform.parent = GameObject.Find("BrainCurve").transform;
            prefab[i].layer = 9;
		}

		InvokeRepeating("generateWave", 1, 1.0f);

	}
		
	void Update () {
		for (int i = 0; i < numOfWaveTotal; i++) {
			waveExpand (i);
		}

		////Radar Effect///

		for (int i = 0; i < numOfWaveTotal * numOfEachWaveTotal; i++) {
			for (int j = 0; j < numOfEachWaveTotal; j++) {
				if (i % numOfEachWaveTotal == j) {
					Renderer rend;
					Color Col;

					rend = prefab [i].GetComponent<Renderer> ();
					Col = rend.material.color;

					if (Mathf.FloorToInt (Time.time * (numOfEachWaveTotal / radarSpeed)) % numOfEachWaveTotal == j) {
						isTint[j] = true;
						Col.a = 1.0f;
					}

					if (isTint[j]) {
						
						if (Col.a > 0.3f) {
							Col.a -= 0.01f;
						}
						if (Col.a <= 0.1f) {
							Col.a = 0.1f;
							isTint [j] = false;
						}
						rend.material.color = Col;
					}

				}

			}

		}
			
	}

	void generateWave()
	{
		if (numWave < numOfWaveTotal) {
			for (int i = 0 + numWave * numOfEachWaveTotal; i < (numOfEachWaveTotal + numWave * numOfEachWaveTotal); i++) {			
				prefab [i].SetActive (true);
				prefab [i].transform.position = new Vector3 (0.0f, -200.0f, 0.0f);
				prefab [i].transform.localScale = new Vector3 (particleSize, particleSize, particleSize);
				particleRad [i] = particleInitRad;
			}

			initOtherZ (numWave);

		}

		if (numWave >= numOfWaveTotal) {
			for (int i = 0 + isRestart * numOfEachWaveTotal; i < (numOfEachWaveTotal + isRestart * numOfEachWaveTotal); i++) {			
				particleRad [i] = particleInitRad;
			}

			initOtherZ (isRestart);

			if (isRestart < (numOfWaveTotal-1)) {
				isRestart++;
			} else
				isRestart -= (numOfWaveTotal-1);
		}
		numWave++;

	}

	void waveExpand(int Num){
		int numUsing = Num;
		//Debug.Log (numUsing);
		for (int i = 0 + numUsing * numOfEachWaveTotal; i < (numOfEachWaveTotal + numUsing * numOfEachWaveTotal); i++) {
			particleRad [i] += Time.deltaTime * Radious_Speed;
			z [i] += Time.deltaTime * Z_Speed * raw_data [i] + 0.01f;

			float x1 = 0.0f + particleRad[i] * Mathf.Cos (360f / numOfEachWaveTotal * i * 3.14f / 180f);
			float y1 = -200.0f + particleRad[i] * Mathf.Sin (360f / numOfEachWaveTotal * i * 3.14f / 180f);

			prefab[i].transform.position = new Vector3 (x1, z[i] , y1);

		}
	}

	void initOtherZ(int num){
		int numUsing = num;

		for (int i = 0 + numUsing * numOfEachWaveTotal; i < (numOfEachWaveTotal + numUsing * numOfEachWaveTotal); i++) {	
			if (i % (numOfEachWaveTotal / 20) == 0) {
				raw_data [i] = UniOSC.UniOSCEventTargetImplementation.delta_raw [(i - numUsing * numOfEachWaveTotal) / (numOfEachWaveTotal / 20)];
				if(float.IsNaN(raw_data[i]) == true){
					raw_data [i] = 0.0f;
				}
			}
		}

		for (int i = 0 + numUsing * numOfEachWaveTotal; i < (numOfEachWaveTotal + numUsing * numOfEachWaveTotal - (numOfEachWaveTotal / 20 - 1)); i++) {
			for (int j = 1; j < (numOfEachWaveTotal / 20); j++) {
				if (i % (numOfEachWaveTotal / 20) == j) {
					raw_data [i] = (raw_data [i + (numOfEachWaveTotal / 20 - j)] - raw_data [i - j]) / (numOfEachWaveTotal / 20) * j + raw_data [i - j];
				}
			}
		}	

		for (int j = 1; j < (numOfEachWaveTotal / 20); j++) {
			raw_data [numOfEachWaveTotal + numUsing * numOfEachWaveTotal - (numOfEachWaveTotal / 20 - j)] = 
				(raw_data [numUsing * numOfEachWaveTotal] - raw_data [numOfEachWaveTotal + numUsing * numOfEachWaveTotal - (numOfEachWaveTotal / 20)]) 
				/ (numOfEachWaveTotal / 20) * j + raw_data [numOfEachWaveTotal + numUsing * numOfEachWaveTotal - (numOfEachWaveTotal / 20)];
		}
		
		for (int i = 0 + numUsing * numOfEachWaveTotal; i < (numOfEachWaveTotal + numUsing * numOfEachWaveTotal); i++) {	
			z [i] =  raw_data [i] * 00.0f;
		}
	}
}

