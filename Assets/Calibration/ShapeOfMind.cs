﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class ShapeOfMind: MonoBehaviour {

	float runTime;
	public float totalTime = 160.0f;//You need get this by time_step_1 + time_step_2 + time_step_3 + 25
	public float time_step_1 = 60.0f;
	public float time_step_2 = 30.0f;
	public float time_step_3 = 40.0f;
	bool showTime = false;
	bool showTitle_1 = false;
	bool showTitle_2 = false;
	bool showTitle_3 = false;
	bool showCurveChoosed = false;
	bool showTitle_4 = false;
	bool showTitle_5 = false;
	bool finishCali = false;
	bool isControl = false;

    private int state = 1;


	string curveChose = "";
	int curveNum = 0;
	int index = 0;
	int CURVEFPS = 60;
	float deltaY = 0.0f;


	bool isCaliStep = false;
	bool isControlStep_1 = false;
	bool isControlStep_2 = false;

	bool isCurveSimilarity = false;
	int TOTALELE = 1800 * 2 + 1;
	int numberOfDrawing = 0;
	float[] numbers = new float[1800 * 2 + 1];

	string[] words = new string[20];

	float[] CurveData1,CurveData2,CurveData3,CurveData4,CurveData5,
			CurveData6,CurveData7,CurveData8,CurveData9,CurveData10,
			CurveData11,CurveData12,CurveData13,CurveData14,CurveData15,
			CurveData16,CurveData17,CurveData18,CurveData19,CurveData20;

	float[] CurveChangeData;

	public GameObject SphereOfCali;
    public GameObject SphereOfCircle;
    public GameObject SphereOfControl1;
    public GameObject LineOfControl1;
    public GameObject SphereOfControl2;
    public GameObject SphereCont2;

	Quaternion rotation,controlrotation;
	Vector3 radius,controlRadiuc;
	float currentRotation = 0.0f;
	float controlRotation = 0.0f;
	float control1 = 0.0f;
	float control2 = 0.0f;

    bool isLarge = false;

    /* */
    public bool calibrate = false;
    private int calibrationStep = 1;

    public CalibrationTextManager textManager;

    public float changeBountary = 0.05f;

    void Awake(){
		if (Display.displays.Length > 1)
			Display.displays[1].Activate();
		if (Display.displays.Length > 2)
			Display.displays[2].Activate();

		Application.targetFrameRate = CURVEFPS;
        QualitySettings.vSyncCount = 0;
    }
    
    public void StartCalibration()
    {
        showTime = false;
        showTitle_1 = false;
        showTitle_2 = false;
        showTitle_3 = false;
        showCurveChoosed = false;
        showTitle_4 = false;
        showTitle_5 = false;
        finishCali = false;
        isControl = false;
        state = 1;

        curveChose = "";
        curveNum = 0;
        index = 0;
        deltaY = 0.0f;

        isCaliStep = false;
        isControlStep_1 = false;
        isControlStep_2 = false;

        isCurveSimilarity = false;

        numberOfDrawing = 0;
        currentRotation = 0.0f;
        controlRotation = 0.0f;
        control1 = 0.0f;
        control2 = 0.0f;

        runTime = totalTime;

        //////////////////Calibration Init//////////////////////
        for (int i = 0; i < numbers.Length - 1; i++)
        {
            if (i % (60) == 0)
            {
                numbers[i] = Random.Range(0, 50);
            }
            numbers[0] = 50;
            numbers[TOTALELE - 1] = 0;
        }


        for (int j = 1; j < (60 * 3); j++)
        {
            for (int i = 0; i < numbers.Length - 1; i++)
            {

                if (i % (60 * 3) == j)
                {
                    numbers[i] = numbers[i - j] + (numbers[i - j + (60 * 3)] - numbers[i - j]) / (60 * 3) * j;
                }
            }
        }

        SaveObject("CaliData.bin", numbers);

        SphereOfCali.SetActive(false);
        SphereOfCircle.SetActive(false);
        
        SphereOfControl1.SetActive(false);
        LineOfControl1.SetActive(false);
        
        SphereOfControl2.SetActive(false);
        SphereCont2.SetActive(false);

        radius = new Vector3(0, 0, 0);
        controlRadiuc = new Vector3(0, 25, 0);

        //print(Application.persistentDataPath);
        words = new string[]{"alpha_relative(0)", "alpha_relative(1)", "alpha_relative(2)", "alpha_relative(3)",
            "beta_relative(0)", "beta_relative(1)", "beta_relative(2)", "beta_relative(3)",
            "delta_relative(0)", "delta_relative(1)", "delta_relative(2)", "delta_relative(3)",
            "gamma_relative(0)", "gamma_relative(1)", "gamma_relative(2)", "gamma_relative(3)",
            "theta_relative(0)", "theta_relative(1)", "theta_relative(2)", "theta_relative(3)"};

        CurveData1 = new float[(int)time_step_1 * CURVEFPS];
        CurveData2 = new float[(int)time_step_1 * CURVEFPS];
        CurveData3 = new float[(int)time_step_1 * CURVEFPS];
        CurveData4 = new float[(int)time_step_1 * CURVEFPS];
        CurveData5 = new float[(int)time_step_1 * CURVEFPS];
        CurveData6 = new float[(int)time_step_1 * CURVEFPS];
        CurveData7 = new float[(int)time_step_1 * CURVEFPS];
        CurveData8 = new float[(int)time_step_1 * CURVEFPS];
        CurveData9 = new float[(int)time_step_1 * CURVEFPS];
        CurveData10 = new float[(int)time_step_1 * CURVEFPS];
        CurveData11 = new float[(int)time_step_1 * CURVEFPS];
        CurveData12 = new float[(int)time_step_1 * CURVEFPS];
        CurveData13 = new float[(int)time_step_1 * CURVEFPS];
        CurveData14 = new float[(int)time_step_1 * CURVEFPS];
        CurveData15 = new float[(int)time_step_1 * CURVEFPS];
        CurveData16 = new float[(int)time_step_1 * CURVEFPS];
        CurveData17 = new float[(int)time_step_1 * CURVEFPS];
        CurveData18 = new float[(int)time_step_1 * CURVEFPS];
        CurveData19 = new float[(int)time_step_1 * CURVEFPS];
        CurveData20 = new float[(int)time_step_1 * CURVEFPS];

        CurveChangeData = new float[(int)time_step_1 * CURVEFPS]; 

        calibrate = true;
    }

	void Update()
	{
        if (!calibrate)
            return;

		runTime -= Time.deltaTime;

		////Running Calibration////////////////////////
		if (runTime > 0.0f) {
			showTime = true;
		}

		if (runTime <= totalTime && runTime > totalTime - 5.0f){
			showTitle_1 = true;
            textManager.Show(0);
        }

		if (runTime <= totalTime - 5.0f && runTime > totalTime - 10.0f) {
            textManager.HideAll();
			showTitle_1 = false;
			showTitle_2 = true;
            textManager.Show(1);

		}

		if (runTime <= totalTime - 10.0f && runTime > totalTime - 10.0f - time_step_1) {
			showTitle_2 = false;
			isCaliStep = true;
			caliStep ();
            textManager.HideAll();
		}

		if (runTime <= totalTime - 10.0f - time_step_1 - 1.0f && runTime > totalTime - 10.0f - time_step_1 - 5.0f){
			showTitle_3 = true;

		}

		if (runTime <= totalTime - 10.0f - time_step_1 - 1.0f && runTime > totalTime - 10.0f - time_step_1 - 2.0f){
			isCaliStep = false;
			finishCali = true;
			compareCurve ();
		}

		if (runTime <= totalTime - 10.0f - time_step_1 - 2.0f && runTime > totalTime - 10.0f - time_step_1 - 5.0f){
				
			showCurveChoosed = true;
				
		}

		if (runTime <= totalTime - 10.0f - time_step_1 - 5.0f && runTime > totalTime - 10.0f - time_step_1 - 10.0f){
			showTitle_3 = false;
			showCurveChoosed = false;
			showTitle_4 = true;
            state = 2;
            textManager.Show(2);
		}

		if (runTime <= totalTime - 10.0f - time_step_1 - 10.0f && runTime > totalTime - 10.0f - time_step_1 - 10.0f - time_step_2){
            textManager.HideAll();
            showTitle_4 = false;
			isControlStep_1 = true;
			isControl = true;
			controlStep_1 ();
		}

		if (runTime <= totalTime - 10.0f - time_step_1 - 10.0f - time_step_2 && runTime > totalTime - 10.0f - time_step_1 - 10.0f - time_step_2 - 5.0f){
			showTitle_5 = true;
			isControlStep_1 = false;
            state = 3;
            textManager.Show(3);
		}

		if (runTime <= totalTime - 10.0f - time_step_1 - 10.0f - time_step_2 - 5.0f && runTime > totalTime - 10.0f - time_step_1 - 10.0f - time_step_2 - 5.0f -time_step_3){
            textManager.HideAll();
            showTitle_5 = false;
			isControlStep_2 = true;
			controlStep_2 ();
		}


		if (runTime <= 0.0f){
			showTime = false;
			isControlStep_2 = false;

			timerEnded();

		}

		////////////////////
		if(isCaliStep == true)
		{	
			SphereOfCali.SetActive(true);
			SphereOfCircle.SetActive(true);
		}
		else if(isCaliStep == false)
		{	
			SphereOfCali.SetActive(false);
			SphereOfCircle.SetActive(false);
		}
		if(isControlStep_1 == true)
		{	
			SphereOfControl1.SetActive(true);
			LineOfControl1.SetActive(true);
		}
		else if(isControlStep_1 == false)
		{	
			SphereOfControl1.SetActive(false);
			LineOfControl1.SetActive(false);
		}
		if(isControlStep_2 == true)
		{	
			SphereOfControl2.SetActive(true);
			SphereCont2.SetActive(true);
		}
		else if(isControlStep_2 == false)
		{	
			SphereOfControl2.SetActive(false);
			SphereCont2.SetActive(false);
		}
		//////////////////////
		if(finishCali == false){
			//stateChange = new Object[] {1};

			for (int i = 0; i < time_step_1 * CURVEFPS - 1 ; i++)
			{
				CurveData1[i] = CurveData1[i+1];
				CurveData2[i] = CurveData2[i+1];
				CurveData3[i] = CurveData3[i+1];
				CurveData4[i] = CurveData4[i+1];
				CurveData5[i] = CurveData5[i+1];
				CurveData6[i] = CurveData6[i+1];
				CurveData7[i] = CurveData7[i+1];
				CurveData8[i] = CurveData8[i+1];
				CurveData9[i] = CurveData9[i+1];
				CurveData10[i] = CurveData10[i+1];
				CurveData11[i] = CurveData11[i+1];
				CurveData12[i] = CurveData12[i+1];
				CurveData13[i] = CurveData13[i+1];
				CurveData14[i] = CurveData14[i+1];
				CurveData15[i] = CurveData15[i+1];
				CurveData16[i] = CurveData16[i+1];
				CurveData17[i] = CurveData17[i+1];
				CurveData18[i] = CurveData18[i+1];
				CurveData19[i] = CurveData19[i+1];
				CurveData20[i] = CurveData20[i+1];
			}


			CurveData1[(int)(time_step_1 * CURVEFPS - 1)] = UniOSC.UniOSCEventTargetImplementation.delta_raw[0];
			CurveData2[(int)(time_step_1 * CURVEFPS - 1)] = UniOSC.UniOSCEventTargetImplementation.delta_raw[1];
			CurveData3[(int)(time_step_1 * CURVEFPS - 1)] = UniOSC.UniOSCEventTargetImplementation.delta_raw[2];
			CurveData4[(int)(time_step_1 * CURVEFPS - 1)] = UniOSC.UniOSCEventTargetImplementation.delta_raw[3];
			CurveData5[(int)(time_step_1 * CURVEFPS - 1)] = UniOSC.UniOSCEventTargetImplementation.delta_raw[4];
			CurveData6[(int)(time_step_1 * CURVEFPS - 1)] = UniOSC.UniOSCEventTargetImplementation.delta_raw[5];
			CurveData7[(int)(time_step_1 * CURVEFPS - 1)] = UniOSC.UniOSCEventTargetImplementation.delta_raw[6];
			CurveData8[(int)(time_step_1 * CURVEFPS - 1)] = UniOSC.UniOSCEventTargetImplementation.delta_raw[7];
			CurveData9[(int)(time_step_1 * CURVEFPS - 1)] = UniOSC.UniOSCEventTargetImplementation.delta_raw[8];
			CurveData10[(int)(time_step_1 * CURVEFPS - 1)] = UniOSC.UniOSCEventTargetImplementation.delta_raw[9];
			CurveData11[(int)(time_step_1 * CURVEFPS - 1)] = UniOSC.UniOSCEventTargetImplementation.delta_raw[10];
			CurveData12[(int)(time_step_1 * CURVEFPS - 1)] = UniOSC.UniOSCEventTargetImplementation.delta_raw[11];
			CurveData13[(int)(time_step_1 * CURVEFPS - 1)] = UniOSC.UniOSCEventTargetImplementation.delta_raw[12];
			CurveData14[(int)(time_step_1 * CURVEFPS - 1)] = UniOSC.UniOSCEventTargetImplementation.delta_raw[13];
			CurveData15[(int)(time_step_1 * CURVEFPS - 1)] = UniOSC.UniOSCEventTargetImplementation.delta_raw[14];
			CurveData16[(int)(time_step_1 * CURVEFPS - 1)] = UniOSC.UniOSCEventTargetImplementation.delta_raw[15];
			CurveData17[(int)(time_step_1 * CURVEFPS - 1)] = UniOSC.UniOSCEventTargetImplementation.delta_raw[16];
			CurveData18[(int)(time_step_1 * CURVEFPS - 1)] = UniOSC.UniOSCEventTargetImplementation.delta_raw[17];
			CurveData19[(int)(time_step_1 * CURVEFPS - 1)] = UniOSC.UniOSCEventTargetImplementation.delta_raw[18];
			CurveData20[(int)(time_step_1 * CURVEFPS - 1)] = UniOSC.UniOSCEventTargetImplementation.delta_raw[19];

			SaveObject("CurveData1.bin", CurveData1);
			SaveObject("CurveData2.bin", CurveData2);
			SaveObject("CurveData3.bin", CurveData3);
			SaveObject("CurveData4.bin", CurveData4);
			SaveObject("CurveData5.bin", CurveData5);
			SaveObject("CurveData6.bin", CurveData6);
			SaveObject("CurveData7.bin", CurveData7);
			SaveObject("CurveData8.bin", CurveData8);
			SaveObject("CurveData9.bin", CurveData9);
			SaveObject("CurveData10.bin", CurveData10);
			SaveObject("CurveData11.bin", CurveData11);
			SaveObject("CurveData12.bin", CurveData12);
			SaveObject("CurveData13.bin", CurveData13);
			SaveObject("CurveData14.bin", CurveData14);
			SaveObject("CurveData15.bin", CurveData15);
			SaveObject("CurveData16.bin", CurveData16);
			SaveObject("CurveData17.bin", CurveData17);
			SaveObject("CurveData18.bin", CurveData18);
			SaveObject("CurveData19.bin", CurveData19);
			SaveObject("CurveData20.bin", CurveData20);

		}

		if(isControl == true){
			int num = CurveChangeData.Length - 1;

			for (int i = 0; i < num; i++) {
				CurveChangeData [i] = CurveChangeData [i + 1];
			}

			//Get the Choosed Curve Data
			CurveChangeData[num] = UniOSC.UniOSCEventTargetImplementation.delta_raw[index];
			//Debug.Log (CurveChangeData[num]);
		}


	}

	void timerEnded()
	{
        calibrate = false;
        GameObject.Find("Calibration").GetComponent<CalibrationView>().CalibrationFinished();
        //Into Next Scence
    }
	void caliStep()
	{
        currentRotation += Time.deltaTime * -(360 / time_step_1);
		radius.y = numbers[numberOfDrawing];
		rotation.eulerAngles = new Vector3(0, 0, currentRotation);
		//Debug.Log (radius.y);
		SphereOfCali.transform.position = rotation * radius;
		if (numberOfDrawing < 1800 * 2)
		{
			numberOfDrawing += 1;
		} else numberOfDrawing = 1800 * 2; 

	}

	void compareCurve()
	{
		//Debug.Log ("CompareStep");
		if(isCurveSimilarity == false)
		{
			
			Vector2[] similarity = new Vector2[20];
			//PVector[] similarity = new PVector[20];
			for(int j = 0; j < 20; j++){
				similarity[j] = CurveSimilarity("CurveData" + (j+1) + ".bin");
			}

			float maxCurveNum = 0.0f;
			for(int i=0; i<20; i++){
				if(Mathf.Max(similarity[i].x,similarity[i].y) >= maxCurveNum)
				{
					maxCurveNum = Mathf.Max(similarity[i].x,similarity[i].y);
					if( similarity[i].x >= similarity[i].y )
					{
						curveChose = "Same" + (i+1);
					}
					else if( similarity[i].x < similarity[i].y ){
						curveChose = "Oppo" + (i+1);  
					}
					else curveChose = "No Curve chosed";

					curveNum = i;
					index = curveNum;
				}
			}
			isCurveSimilarity = true;
		}
	}
	void controlStep_1()
	{
        float positionY = SphereOfControl1.transform.position.y;
        float changeOrNot = CurveChange();

        if (changeOrNot == 1.0f)
        {
            control1 = 0.05f;
        }

        if (changeOrNot == -1.0f)
        {
            control1 = -0.05f;
        }
        deltaY = control1 * 1.0f;

        Vector3 deltaPos = new Vector3(0, deltaY, 0);

        if (positionY >= -40.0f && positionY <= 50.0f)
        {
            SphereOfControl1.transform.position -= deltaPos;
        }
        else if (positionY < -40.0f)
        {
            SphereOfControl1.transform.position = new Vector3(0, -40.0f, 0);
        }
        else if (positionY > 50.0f)
        {
            SphereOfControl1.transform.position = new Vector3(0, 50.0f, 0);
        }

    }

	void controlStep_2()
	{
        controlRotation += Time.deltaTime * -(360 / time_step_3);
        float changeOrNot = CurveChange();

        if (changeOrNot == 1.0f)
        {
            control2 = 0.05f;
        }

        if (changeOrNot == -1.0f)
        {
            control2 = -0.05f;
        }

        if (controlRadiuc.y < 0.0f)
        {
            controlRadiuc.y = 0.0f;

        }

        if (controlRadiuc.y > 50.0f)
        {
            controlRadiuc.y = 50.0f;

        }

        if (controlRadiuc.y <= 50.0f && controlRadiuc.y >= 0.0f)
        {
            controlRadiuc.y -= control2;

        }

        controlrotation.eulerAngles = new Vector3(0, 0, controlRotation);
        SphereOfControl2.transform.position = controlrotation * controlRadiuc;
    }

    Vector2 CurveSimilarity(string fileName)
	{
		string filename = fileName;
		float[] test2 = new float[300];
		float[] CaliData = new float[3600];

		int CaliNumDistant = 60;
		int totalCaliNum = 3600 / CaliNumDistant;

		float deltaBorP = 0.3f;
		float deltaBorPCurve2 = 0.03f;
		int[] CaliBorP = new int[60];
		int[] Curve2BorP = new int[60];

		float[] CaliData_just, test2_just;

		int SameNum = 0;
		int OppoNum = 0;

		Vector2 SameOrOppo = new Vector2();

		test2 = (float[]) LoadObject(filename);
		CaliData = (float[]) LoadObject("CaliData.bin");

		CaliData_just = new float[totalCaliNum + 1];
		CaliData_just[totalCaliNum] = 0;
		for (int i=0; i< (CaliData.Length); i++) {
			if (i % CaliNumDistant == 0) {
				CaliData_just[i / CaliNumDistant] = CaliData[i];     
			}    
		}
			
		test2_just = new float[totalCaliNum + 1];
		test2_just[totalCaliNum] = 0;
		for (int i=0; i< (test2.Length); i++) {
			if (i % (test2.Length/totalCaliNum) == 0) {
				test2_just[i / (test2.Length/totalCaliNum)] = test2[i];     
			}    
		}


		//Bottom and Peak
		//Cali
		for (int i=1; i< (CaliData_just.Length-1); i++) {
			if((CaliData_just[i]-CaliData_just[i+1])+(CaliData_just[i]-CaliData_just[i-1]) > (deltaBorP*50) && 
				(CaliData_just[i]-CaliData_just[i+1])>0 && (CaliData_just[i]-CaliData_just[i-1])>0){
				CaliBorP[i] = -1;
			}
			else if ((CaliData_just[i+1]-CaliData_just[i])+(CaliData_just[i-1]-CaliData_just[i]) > (deltaBorP*50) && 
				(CaliData_just[i+1]-CaliData_just[i])>0 && (CaliData_just[i-1]-CaliData_just[i])>0){
				CaliBorP[i] = 1;
			}
			else CaliBorP[i] = 0;
		}
		CaliBorP[0] = 0;
		CaliBorP[59] =0;

		//Curve 2 
		for (int i=1; i< (CaliData_just.Length-1); i++) {
			if((test2_just[i]-test2_just[i+1])+(test2_just[i]-test2_just[i-1]) > (deltaBorPCurve2*1) && 
				(test2_just[i]-test2_just[i+1])>0 && (test2_just[i]-test2_just[i-1])>0){
				Curve2BorP[i] = -1;
			}
			else if ((test2_just[i+1]-test2_just[i])+(test2_just[i-1]-test2_just[i]) > (deltaBorPCurve2*1) && 
				(test2_just[i+1]-test2_just[i])>0 && (test2_just[i-1]-test2_just[i])>0){
				Curve2BorP[i] = 1;
			}
			else Curve2BorP[i] = 0;
		}
		Curve2BorP[0] = 0;
		Curve2BorP[59] =0;

		for (int i=0; i< (CaliBorP.Length); i++) {

			//print(CaliBorP[i] + " " + Curve2BorP[i]);
			if((CaliBorP[i] + Curve2BorP[i]==2)||(CaliBorP[i] + Curve2BorP[i]==-2)){
				SameNum++;
			}
			else if ((CaliBorP[i] + Curve2BorP[i]==0) && CaliBorP[i]!=0 && Curve2BorP[i]!=0){
				OppoNum++;
			}
		}   

		SameOrOppo.x = SameNum;
		SameOrOppo.y = OppoNum;

		return(SameOrOppo);
	}

	void SaveObject(string fileName, float[] toSave)
	{
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/" + fileName);

		float[] data = new float[1800 * 2 + 1] ;
		data = toSave;

		bf.Serialize(file, data);
		file.Close();
	}

	float[] LoadObject(string fileName)
	{
		float[] readVal = new float[1800 * 2 + 1];
		
		if(File.Exists(Application.persistentDataPath + "/" + fileName))
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/" + fileName, FileMode.Open);

			readVal = (float[])bf.Deserialize(file);
			file.Close();

		}
		return readVal;
	} 

	public float CurveChange()
	{
        int num = (int)time_step_1 * CURVEFPS - 1;
        float changeOrNot = 0.0f;

        if (CurveChangeData[num] - CurveChangeData[num - 150] > changeBountary && isLarge == false)
        {
            changeOrNot = 1.0f;
            isLarge = true;
        }
        if (CurveChangeData[num] - CurveChangeData[num - 150] > changeBountary && isLarge == true)
        {
            isLarge = true;

        }
        if (CurveChangeData[num] - CurveChangeData[num - 150] < changeBountary && isLarge == true)
        {
            changeOrNot = -1.0f;
            isLarge = false;
        }
        if (CurveChangeData[num] - CurveChangeData[num - 150] < changeBountary && isLarge == false)
        {
            isLarge = false;

        }

        return changeOrNot;
    }


    public float GetDecision()
    {
        float val = UniOSC.UniOSCEventTargetImplementation.delta_raw[index];
        if (val > 0.5f) return 1;
        if (val < 0.5f) return -1;

        return 0;
    }

    public int GetState()
    {
        return state;
    }

    public float GetWave()
    {
        return UniOSC.UniOSCEventTargetImplementation.delta_raw[index];
    }

    public float GetDistState1()
    {
        return Vector3.Distance(SphereOfCircle.transform.position, SphereOfCali.transform.position);
    }

    public float GetDistState2()
    {
        return Vector3.Distance(SphereOfControl1.transform.position, LineOfControl1.transform.position);
    }

    public float GetDistState3()
    {
        return Vector3.Distance(SphereOfControl2.transform.position, SphereCont2.transform.position);
    }
}