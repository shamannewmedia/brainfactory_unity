using UnityEngine;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using Ventuz.OSC;

namespace UniOSC{

    public class UniOSCEventTargetImplementation : MonoBehaviour {
        public static float[] delta_raw;

        public OSCReceiverThreaded oscMuse;

        private List<OscElement> receivedMessages;
        private List<OscElement> forehead;

        public int messagesAmount = 50;

        public float currentMessageKeepAlive = 0.0f;
        private float maxCurrentMessageKeepAlive = 2.0f;
        private float minCurrentMessageKeepAlive = 0.0f;

        private float keepAliveIncrementalStep = 0.35f;
        private float keepAliveDecrementalStep = 0.08f;// 0.01f;
        private float timeToIdle = 3.0f;
        private bool receivedHeadMessage = false;

        public float timeTowait = 0.2f;
        private float timeWaiting = 0.0f;

        //public GameObject receivetest = null;

        public void StartReceiving(int port)
        {
            keepAliveDecrementalStep = timeTowait / timeToIdle;
            currentMessageKeepAlive = 0.0f;
            receivedHeadMessage = false;

            receivedMessages = new List<OscElement>();
            forehead = new List<OscElement>();
            oscMuse.StartListening(port);
            DeltaRawInitialization();

        }

        public void DeltaRawInitialization()
        {
            delta_raw = new float[20];
            for (int i = 0; i < 20; i++)
            {
                delta_raw[i] = 0.0f;

            }
        }
        
        void Update()
        {
            List<OscElement> elements = oscMuse.getElementsInQueue(messagesAmount);
            
            foreach (OscElement message in elements)
                OnOSCMessageReceived(message);


            timeWaiting += Time.deltaTime;
            if (timeWaiting >= timeTowait)
            {
                if (receivedHeadMessage)
                {
                    currentMessageKeepAlive += keepAliveIncrementalStep;

                    if (currentMessageKeepAlive > maxCurrentMessageKeepAlive)
                    {
                        currentMessageKeepAlive = maxCurrentMessageKeepAlive;
                    }
                }
                else
                {
                    currentMessageKeepAlive -= keepAliveDecrementalStep;

                    if (currentMessageKeepAlive < minCurrentMessageKeepAlive)
                    {
                        currentMessageKeepAlive = minCurrentMessageKeepAlive;
                    }
                }

                timeWaiting = 0.0f;
                receivedHeadMessage = false;
            }
        }

		private void OnOSCMessageReceived(OscElement args){
            
			if ( args.Address == "/muse/elements/alpha_relative" )
            {
				for(int j = 0; j < 4; j++){
					delta_raw[j] = (float)args.Args[j];
				}
			}
			if ( args.Address == "/muse/elements/beta_relative" )
            {
				for(int j = 4; j < 8; j++){
					delta_raw[j] = (float)args.Args[j-4];
				}
			}
			if ( args.Address == "/muse/elements/delta_relative" )
            {
				for(int j = 8; j < 12; j++){
					delta_raw[j] = (float)args.Args[j-8];
				}
			}
			if ( args.Address == "/muse/elements/gamma_relative" )
            {
				for(int j = 12; j < 16; j++){
					delta_raw[j] = (float)args.Args[j-12];
				}
			}

			if ( args.Address == "/muse/elements/theta_relative" )
            {
				for(int j = 16; j < 20; j++){
					delta_raw[j] = (float)args.Args[j-16];
				}
			}

            if (args.Address == "/muse/elements/touching_forehead")
            {
                if ((int)args.Args[0] == 1)
                {
                    receivedHeadMessage = true;
                }
                forehead.Add(args);
            }

        }

    }

}