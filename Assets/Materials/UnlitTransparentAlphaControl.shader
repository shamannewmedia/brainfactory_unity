﻿Shader "Custom/UnlitTransparentAlphaControl" {
	Properties {
		_MainTex ("Main Texture", 2D) = "white" {}
		_Alpha ("Alpha", Range(0.0, 1.0)) = 0.0
	}
	SubShader {
		Tags { 	
				"Queue" = "Transparent"
				"RenderType"="Opaque"
			 }
		LOD 100
		
		Lighting Off
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off
		ZTest LEqual
		
		Pass{
		CGPROGRAM

			#pragma vertex vert             
			#pragma fragment frag
			
			// Use shader model 3.0 target, to get nicer looking lighting
			//#pragma target 3.0
			
			sampler2D _MainTex;
			float _Alpha;

			struct vertInput {
				float2 uv : TEXCOORD0;
				float4 pos : POSITION;
			};  
			 
			struct vertOutput {
				float2 uv : TEXCOORD0; // texture coordinate
				float4 pos : SV_POSITION;
			};

			vertOutput vert(
							float4 vertex : POSITION, // vertex position input
                			float2 uv : TEXCOORD0 // first texture coordinate input
                			) {
				vertOutput o;
				o.pos = mul(UNITY_MATRIX_MVP, vertex);
				o.uv = uv;
				return o;
			}
			
			half4 frag(vertOutput output) : COLOR {
				half4 maintex = tex2D(_MainTex, output.uv);
				half4 final=maintex;
				if(maintex.w>_Alpha){
					final.w = _Alpha;
				}
				return final; 
			}
			ENDCG
		}
	} 
	FallBack "Unlit/Transparent"
}
