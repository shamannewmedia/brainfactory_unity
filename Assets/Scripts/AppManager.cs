﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Ventuz.OSC;
using System.Xml;
using System.IO;
using Thinksquirrel.Fluvio;
using WhiteCat.Paths;
using System;
using UniOSC;

public class AppManager : MonoBehaviour {

    public enum AppState {Idle, Calibrating, ShowConcept, Shaping, Processing, End, Error};

    public bool useTCPConnection;

    public OSCSender sender;
    private OSCReceiverThreaded OSCInput;
    private AppState currentState;

    private List<OscElement> OSCInputBuffer;
    public int maxMessagesToProcess = 10;

    public FluidManager fluidM;


    private List<DNAInstance> DNAInstances;
    private int currentDNAInstanceIndex;

    private List<DNAModifier> DNAMods;
    private int currentDNAModifierIndex;
    public float shapingWindowTime = 200f; //seconds
    public float shapingStartTime = 0.0f; //seconds
    public BezierPath[] splines;

    private int keepModifiers = 0;

    //XML settings file
    private XmlDocument doc;
    public string xmlPath = "settings.xml";
    //End XML Settings file

    private bool loadingOK;
    

    private string basePath;

    //GUI section

    public UILabel CurrentStateTitle;
    public UILabel CurrentShapingTimer;
    public UILabel CurrentDNAInstanceName;
    public UILabel CurrentDNAName;
    public UILabel CurrentDNAValue;

    //End GUI Section

    //XML Interactions section
    private XmlDocument interactionsXML;
    private XmlDocument interactionsNode;
    //End XML section

    public GameObject calibrationView;
    public GameObject brainCurveView;
    public GameObject dummyCam;
    public GameObject fluidView;

    public GameObject shapeOfMind;
    public UniOSCEventTargetImplementation uniosc;

    public TextManager textManager;

    public GameObject verbose;
    public GameObject modifierName;
    private bool showModifierName = false;

    public Blinking blinkingRecording;

    /* Time the muse has to be on the head for things to happen*/
    private float museTimeToStart = 2f;
    private float museTimeToEnd = 4f;
    private bool museOn = false;
    private float museTime = 0f;


    private bool showBrainWave;


    private void LoadFromXML() {

        this.doc = new XmlDocument();

        if (File.Exists(xmlPath)) {

            try {
                this.doc.Load(xmlPath);

                XmlNode settings = doc.SelectSingleNode("app_settings/Settings");
                XmlNode conceptText = doc.SelectSingleNode("app_settings/ConceptText");
                XmlNode oscOutputDefinition = doc.SelectSingleNode("app_settings/OSCOutPut");
                XmlNode museInput = doc.SelectSingleNode("app_settings/MuseOSCInput");
                XmlNode museTimes = doc.SelectSingleNode("app_settings/MuseTime");
                XmlNodeList oscInputDefinition = doc.SelectNodes("app_settings/OSCInput");
                XmlNodeList exportTCPDefinition = doc.SelectNodes("app_settings/ExportSettings/TCPServer");
                XmlNodeList exportPathDefinition = doc.SelectNodes("app_settings/ExportSettings/ExportPath");
                XmlNodeList modifiersDefinition = doc.SelectNodes("app_settings/DNAModifiers/DNAModifier");
                XmlNodeList captureDefinition = doc.SelectNodes("app_settings/ExportSettings/CaptureSettings");
                XmlNodeList dnaInstancesDefinition = doc.SelectNodes("app_settings/DNAInstances/DNAInstance");
                XmlNodeList pointEffectors = doc.SelectNodes("app_settings/Effectors/Point");
                XmlNodeList ellipsoidEffectors = doc.SelectNodes("app_settings/Effectors/Ellipsoid");
                XmlNodeList cubeEffectors = doc.SelectNodes("app_settings/Effectors/Cube");
                XmlNodeList systemEffectors = doc.SelectNodes("app_settings/Effectors/System");
                XmlNodeList emitters = doc.SelectNodes("app_settings/Emitters/Emitter");
                // setting up OSCInput and Output

                uniosc.StartReceiving(int.Parse(museInput.Attributes["Port"].Value));
                OSCInput.StartListening(int.Parse(oscInputDefinition[0].Attributes["Port"].Value));
                //this.OSCInput.listeningPort = int.Parse(oscInputDefinition[0].Attributes["Port"].Value);

                string ipAddress = oscOutputDefinition.Attributes["IP"].Value;
                int port = int.Parse(oscOutputDefinition.Attributes["Port"].Value);

                

                if (museTimes.Attributes["Start"] != null)
                    museTimeToStart = float.Parse(museTimes.Attributes["Start"].Value);

                if (museTimes.Attributes["End"] != null)
                    museTimeToEnd = float.Parse(museTimes.Attributes["End"].Value);



                shapingWindowTime = float.Parse(settings.Attributes["ShapingTime"].Value);
                if (settings.Attributes["ModifiersToKeep"] != null)
                    keepModifiers = int.Parse(settings.Attributes["ModifiersToKeep"].Value);
                if (settings.Attributes["BrainWave"] != null)
                {
                    if (settings.Attributes["BrainWave"].Value.ToLower().Equals("true"))
                    {
                        this.showBrainWave = true;
                    }
                    else
                    {
                        this.showBrainWave = false;
                    }
                }
                if (settings.Attributes["ShowModifierName"] != null)
                    showModifierName = settings.Attributes["ShowModifierName"].Value.ToLower() == "true" ? true : false;

                bool record = false;
                if (settings.Attributes["Recording"] != null)
                    record = settings.Attributes["Recording"].Value.ToLower() == "true" ? true : false;

                GameObject.Find("SenderManager").GetComponent<OSCSender>().SetParameters(port, ipAddress, record);

                string englishText = conceptText.Attributes["English"].Value;
                string koreanText = conceptText.Attributes["Korean"].Value;
                float textTime = float.Parse(conceptText.Attributes["TextTime"].Value);
                float conceptTime = float.Parse(conceptText.Attributes["ConceptTime"].Value);
                float shapingTextTime = float.Parse(conceptText.Attributes["ShapingTime"].Value);

                textManager.SetValues(englishText, koreanText, conceptTime, textTime, shapingTextTime);


                // setting up TCP and Export settings

                this.fluidM.IP = exportTCPDefinition[0].Attributes["IP"].Value;
                this.fluidM.Port = int.Parse(exportTCPDefinition[0].Attributes["Port"].Value);
                this.useTCPConnection = bool.Parse(exportTCPDefinition[0].Attributes["UseTCP"].Value);
                this.fluidM.SetBasePath(exportPathDefinition[0].Attributes["Path"].Value);
                GameObject.Find("SenderManager").GetComponent<OSCSender>().SetBasePath(exportPathDefinition[0].Attributes["Path"].Value);
                this.basePath = exportPathDefinition[0].Attributes["Path"].Value;

                //Capture Settings

                this.fluidM.FrameCaptureQuantity = int.Parse(captureDefinition[0].Attributes["FrameQuantity"].Value);
                this.fluidM.FramesToSkipBetweenCapture = int.Parse(captureDefinition[0].Attributes["FramesBetweenCaptures"].Value);

                //Creating emitters
                fluidM.CreateEmitters(emitters);

                //Creating effectors
                if (!fluidM.CreateEffectors(pointEffectors, "point"))
                    loadingOK = false;

                if (!fluidM.CreateEffectors(ellipsoidEffectors, "ellipsoid"))
                    loadingOK = false;

                if (!fluidM.CreateEffectors(cubeEffectors, "cube"))
                    loadingOK = false;

                if (!fluidM.CreateSystems(systemEffectors))
                    loadingOK = false;

                fluidM.EffectorsLists();


                //Creating modifiers
                if (modifiersDefinition.Count > 0)
                {
                    for (int i = 0; i < modifiersDefinition.Count; i++)
                    {

                        string modType = modifiersDefinition[i].Attributes["Type"].Value;

                        switch (modType)
                        {
                            case "Ellipsoid":
                                DNAModifierEllipsoid dE = new DNAModifierEllipsoid(modifiersDefinition[i]);
                                this.DNAMods.Add(dE);
                                break;

                            case "Point":
                                DNAModifierPoint dP = new DNAModifierPoint(modifiersDefinition[i]);
                                this.DNAMods.Add(dP);
                                break;

                            case "Fluid":
                                DNAModifierFluid dF = new DNAModifierFluid(modifiersDefinition[i]);
                                this.DNAMods.Add(dF);
                                break;

                            case "System":
                                DNAModifierSystem dS = new DNAModifierSystem(modifiersDefinition[i]);
                                DNAMods.Add(dS);
                                break;

                            case "Emitter":
                                DNAModifierEmitter dEm = new DNAModifierEmitter(modifiersDefinition[i]);
                                DNAMods.Add(dEm);
                                break;
                            
                            default:
                                Debug.Log("Invalid Modifier Type");
                                break;
                        }

                    }
                }
                else
                {
                   // loadingOK = false;
                }

                //Creating DnaInstances

                if (dnaInstancesDefinition.Count > 0)
                {
                    for (int i = 0; i < dnaInstancesDefinition.Count; i++)
                    {
                        DNAInstance dInstance = new DNAInstance(dnaInstancesDefinition[i]);
                        this.DNAInstances.Add(dInstance);
                    }
                }
                else{
                    loadingOK = false;
                }


            }
            catch (System.Exception e) {
                Debug.Log(e.Message);
                loadingOK = false;
            }



        } else {
            loadingOK = false;
        }

    }

   
    // Use this for initialization
    void Start() {
        //we need this to run in background
        Application.runInBackground = true;
        Screen.fullScreen = true;

        showBrainWave = false;
        brainCurveView.SetActive(false);
        dummyCam.SetActive(true);

        OSCInput = this.gameObject.GetComponent<OSCReceiverThreaded>();
        //OSCOutput = this.gameObject.GetComponent<OSCSenderThreaded>();

        OSCInputBuffer = new List<OscElement>();
        DNAInstances = new List<DNAInstance>();

        DNAMods = new List<DNAModifier>();

        //loading from xml
        loadingOK = true;
        this.xmlPath = Application.dataPath + "/Resources/" + this.xmlPath;
        Debug.Log(xmlPath);

        LoadFromXML();

        if (loadingOK) {

            //normal operation if loading OK.

            CreateInteractionsXML ();
            
            if (useTCPConnection) {
                fluidM.ConnectToServer();
            }

            GameObject.Find("SenderManager").GetComponent<OSCSender>().StartSending();


            currentState = AppState.Idle;
            ChangeAppState(AppState.Idle);
        } else {
            //loading went wrong.
            //we set the app state to error, and this may only be reversed by restarting the app.
            this.ChangeAppState(AppState.Error);
            UpdateGUILabelsForError();
        }

        currentDNAInstanceIndex = -1;
        IncrementDNAInstanceIndex();

        currentDNAModifierIndex = -1;
    }

    // Update is called once per frame
    void Update() {

        if (!loadingOK)
            return; 

        //we only do stuff if the loading went OK during startup.
        getOSCInputMessages();
        ProcessInputMessages();
        UpdateGUILabels();

        switch (currentState)
        {
            case AppState.Shaping:
                if (Time.realtimeSinceStartup - this.shapingStartTime < this.shapingWindowTime)
                {
                    if (currentDNAModifierIndex >= 0 && currentDNAModifierIndex < DNAMods.Count)
                    {
                        DNAMods[currentDNAModifierIndex].Modify();
                    }

                    //we do current shaping of DNA
                    //message processing is being done in the ProcessInputMssages() method.

                }
                else
                {
                    //changing DNA
                    AdvanceDNA();
                }

                break;

            case AppState.Processing:
                if (!fluidM.GetIsProcessing())
                {
                    //Processing has ended
                    string FileName = fluidM.GetLastProcessedFileName();

                    //saving current interaction information to file.
                    this.AddInteractionXMLNode(FileName);

                    //saving current DNA Instance configuration to the settings file
                    SaveDNAToSettingsXML();

                    //Changing to end
                    this.ChangeAppState(AppState.End);

                }
                break;
        }

            
        if (Input.GetKeyUp("s"))
            this.fluidM.SendStopProcessing();

        if (Input.GetKeyUp("a"))
            Screen.fullScreen = !Screen.fullScreen;

        if (Input.GetKeyUp(KeyCode.Space))
            NextState();       

        if (currentState == AppState.Idle)
        {
            if (uniosc.currentMessageKeepAlive >=1.0f)
                StartAllThings();
        }
        else
        {
            if(currentState == AppState.End)
            {
                if (uniosc.currentMessageKeepAlive < 1.0f)
                    StopAllThings();
            }
            else
            {
                if (uniosc.currentMessageKeepAlive < 1.0f)
                    StopAllThings();
            }
            
        }

    }

    private void AdvanceDNA()
    {
        //we advance DNA shaping, or we go to processing
        this.shapingStartTime = Time.realtimeSinceStartup;
        this.currentDNAModifierIndex += 1;

        //GameObject.Find("Verbose").GetComponent<Indicators>().ChangeEffector(DNAMods[currentDNAModifierIndex].GetDNAId());
        Debug.Log("Changing DNA mod. New index:" + currentDNAModifierIndex.ToString());

        if (currentDNAModifierIndex > this.DNAMods.Count - 1)
        {
            //we go to processing
            this.currentDNAModifierIndex = 0;
            this.ChangeAppState(AppState.Processing);
            return;
        }

        modifierName.GetComponent<Text>().text = DNAMods[currentDNAModifierIndex].GetDNAId();
        verbose.GetComponent<Indicators>().ChangeEffector(DNAMods[currentDNAModifierIndex].GetEffectorName());
    }

    public void ModifierDone()
    {
        AdvanceDNA();
    }

    private void UpdateGUILabels(){

        try
        {

            switch (this.currentState)
            {
                case AppState.Idle:

                    this.CurrentStateTitle.text = "Idle";
                    this.CurrentDNAInstanceName.text = this.DNAInstances[this.currentDNAInstanceIndex].GetName();
                    this.CurrentDNAName.text = "N/A";
                    this.CurrentShapingTimer.text = "N/A";
                    this.CurrentDNAValue.text = "N/A";

                    break;
                case AppState.Calibrating:

                    this.CurrentStateTitle.text = "Calibrating";
                    this.CurrentDNAInstanceName.text = this.DNAInstances[this.currentDNAInstanceIndex].GetName();
                    this.CurrentDNAName.text = "N/A";
                    this.CurrentShapingTimer.text = "N/A";
                    this.CurrentDNAValue.text = "N/A";
                                        
                    break;
                case AppState.ShowConcept:

                    this.CurrentStateTitle.text = "Showing concept";
                    this.CurrentDNAInstanceName.text = this.DNAInstances[this.currentDNAInstanceIndex].GetName();
                    this.CurrentDNAName.text = "N/A";
                    this.CurrentShapingTimer.text = "N/A";
                    this.CurrentDNAValue.text = "N/A";

                    break;

                case AppState.Shaping:

                    this.CurrentStateTitle.text = "Shaping";
                    this.CurrentDNAInstanceName.text = this.DNAInstances[this.currentDNAInstanceIndex].GetName();
                    if (currentDNAModifierIndex >= 0) { 
                        this.CurrentDNAName.text = this.DNAMods[currentDNAModifierIndex].GetDNAId();
                        this.CurrentShapingTimer.text = (this.shapingWindowTime - (Time.realtimeSinceStartup-this.shapingStartTime)).ToString();
                        this.CurrentDNAValue.text = this.DNAMods[currentDNAModifierIndex].GetCurrentValue().ToString();
                    }

                    break;
                case AppState.Processing:

                    this.CurrentStateTitle.text = "Processing";
                    this.CurrentDNAInstanceName.text = this.DNAInstances[this.currentDNAInstanceIndex].GetName();
                    this.CurrentDNAName.text = "N/A";
                    this.CurrentShapingTimer.text = "N/A";
                    this.CurrentDNAValue.text = "N/A";

                    break;
                case AppState.Error:

                    this.CurrentStateTitle.text = "Error";
                    this.CurrentDNAInstanceName.text = this.DNAInstances[this.currentDNAInstanceIndex].GetName();
                    this.CurrentDNAName.text = "N/A";
                    this.CurrentShapingTimer.text = "N/A";
                    this.CurrentDNAValue.text = "N/A";

                    break;
                case AppState.End:

                    this.CurrentStateTitle.text = "End";
                    this.CurrentDNAInstanceName.text = this.DNAInstances[this.currentDNAInstanceIndex].GetName();
                    this.CurrentDNAName.text = "N/A";
                    this.CurrentShapingTimer.text = "N/A";
                    this.CurrentDNAValue.text = "N/A";

                    break;
                default:
                    this.CurrentStateTitle.text = "Unknown state";
                    this.CurrentDNAInstanceName.text = this.DNAInstances[this.currentDNAInstanceIndex].GetName();
                    this.CurrentDNAName.text = "N/A";
                    this.CurrentShapingTimer.text = "N/A";
                    this.CurrentDNAValue.text = "N/A";
                    break;
            }

        }
        catch (System.Exception e)
        {
            Debug.Log("Error updating GUI: " + e.Message);
        }

    }

    private void UpdateGUILabelsForError()
    {

        this.CurrentStateTitle.text = "Error";
        this.CurrentDNAName.text = "N/A";
        this.CurrentShapingTimer.text = "N/A";
        this.CurrentDNAValue.text = "N/A";

    }
    /*
    public void SendGenericOSCMessage(OscBundle m){
		this.OSCOutput.storeElementInQueue (m);
	}
    */

	private void getOSCInputMessages(){
        //we get OSCMessages
        OscElement oscM = this.OSCInput.getFirstElementInQueue();
        if (oscM != null)
		{
			int messageCounter = 0;
			while (oscM != null && messageCounter < maxMessagesToProcess)
			{
				//Debug.Log ("Message Received.");
				messageCounter += 1;
				this.OSCInputBuffer.Add(oscM);
				oscM = this.OSCInput.getLastElementInQueue();
			}
		}
	}

	private void ProcessInputMessages (){

        // BRAIN INPUT
        if (currentState == AppState.Shaping)
        {
            float val = shapeOfMind.GetComponent<ShapeOfMind>().GetDecision();
            DNAModifier currentMod = this.DNAMods[this.currentDNAModifierIndex];
            currentMod.SetValue(val);
        }
        //CONTROL INPUT

        if (this.OSCInputBuffer.Count > 0) {
			//getting and removing the message
			OscElement message = this.OSCInputBuffer[0];
			OSCInputBuffer.RemoveAt(0);

            if (message.Address == "/Start")
            {
                if (this.currentState == AppState.Shaping)
                {
                    //we change to next DNA or switch to Processing state.

                    //Debug.Log("Message Processed: Change DNA");
                    AdvanceDNA();

                }
            }

            if (message.Address == "/StateChange") {
                NextState(); 
            }
        }

	}

    private void NextState()
    {
        switch (currentState)
        {
            case AppState.Idle:
                ChangeAppState(AppState.Calibrating);
                break;

            case AppState.Calibrating:
                ChangeAppState(AppState.Shaping);
                break;

            case AppState.Shaping:
                ChangeAppState(AppState.Processing);
                break;

            case AppState.Processing:
                ChangeAppState(AppState.Idle);
                break;
        }
    }

	private void ChangeAppState(AppState toState)
    {

        if (toState != AppState.Shaping)
            modifierName.SetActive(false);
        else
            modifierName.SetActive(showModifierName);

        try
        {
            AppState prevState = currentState;
            currentState = toState;
            switch (toState)
			{
			    case AppState.Idle:
                    blinkingRecording.HideBlinking();
                    currentDNAModifierIndex = -1;

                    if (prevState == AppState.End)
                    {
                        textManager.HideShapingDone();
                    }

                    IncrementDNAInstanceIndex();

                    calibrationView.GetComponent<CalibrationView>().Hide();
                    //starting fluid simulations
                    fluidM.PlayMainFluid();

                    fluidView.GetComponent<FluidView>().Show();

                    //increment DNA Instance

                    if (showBrainWave)
                    {
                        this.brainCurveView.SetActive(false);
                        this.dummyCam.SetActive(true);
                    }

                    sender.ClearRecords();

                    break;
			    case AppState.Calibrating:
                    blinkingRecording.ShowBlinking();

                    fluidView.GetComponent<FluidView>().Hide();

                    //stopping fluid simulations
                    fluidM.StopMainFluid();

                    calibrationView.GetComponent<CalibrationView>().Show();

                    calibrationView.GetComponent<CalibrationView>().StartCalibrating();

                    //museOn = true;

                    if (showBrainWave)
                    {
                        this.brainCurveView.SetActive(true);
                        this.dummyCam.SetActive(false);
                    }

                    sender.StartRecording();
                    break;

			    case AppState.Shaping:
                    calibrationView.GetComponent<CalibrationView>().Hide();
                    //currentDNAModifierIndex = 0;
                    AdvanceDNA();

                    fluidM.PlayMainFluid();

                    textManager.ShowText(DNAInstances[currentDNAInstanceIndex].GetName(), DNAInstances[currentDNAInstanceIndex].GetAmount(), DNAInstances[currentDNAInstanceIndex].GetKoreanName());
                    Debug.Log("Change to Shaping");

                    shapingStartTime = Time.realtimeSinceStartup;
                    fluidView.GetComponent<FluidView>().Show();
                    //starting fluid simulations

                    if (showBrainWave)
                    {
                        this.brainCurveView.SetActive(false);
                        this.dummyCam.SetActive(true);
                    }

                    //modifierName.SetActive(showModifierName);

                    break;

			    case AppState.Processing:
                    blinkingRecording.HideBlinking();
                    textManager.ShowShapingDone();

                    //we start to capture the fluid at the FluidManager
                    this.SaveDNAValuesToCurrentDNAInstance();
                
                    fluidM.ProcessParticles();
                    Debug.Log("Begin FluidCapture");

                    if (showBrainWave)
                    {
                        this.brainCurveView.SetActive(false);
                        this.dummyCam.SetActive(true);
                    }

                    sender.StopRecording();
                    sender.StoreRecords();

                    break;

                case AppState.Error:

                    //we start to capture the fluid at the FluidManager
                    Debug.Log("Error");

                    if (showBrainWave)
                    {
                        this.brainCurveView.SetActive(false);
                        this.dummyCam.SetActive(true);
                    }

                    break;

                case AppState.ShowConcept:
                    fluidM.StopMainFluid();

                    if (showBrainWave)
                    {
                        this.brainCurveView.SetActive(false);
                        this.dummyCam.SetActive(true);
                    }

                    break;

                default:
				    Debug.Log("Invalid state change");
				    break;
			}

		}
		catch(System.Exception e){
			Debug.Log ("Unable to perform state change: " + e.Message);
		}

	}
    
    public FluidParticleSystem GetFluid ()
    {
        return fluidM.GetFluid();
    }

	public BezierPath GetSplineByIdentifier (int splineId){

		BezierPath result = null;

		if (splineId <= this.splines.Length - 1) {
			result = this.splines [splineId];
		}

		return result;
	}

    private void SaveDNAValuesToCurrentDNAInstance()
    {
        //saving current values to current modifier
        //updating DNAModifierValues to the ones stored at the new current DNAInstance

        foreach (DNAModifier dM in this.DNAMods)
        {
            //this.DNAInstances[currentDNAInstanceIndex].EditDNAModifierValue(dM.GetDNAId(), dM.GetCurrentNormalizedValue());
            this.DNAInstances[currentDNAInstanceIndex].EditDNAModifierValue(dM.GetDNAId(), dM.GetCurrentValue());
        }
    }

    private void IncrementDNAInstanceIndex()
    {
        if (DNAInstances.Count == 0)
            return;
        
        currentDNAInstanceIndex = UnityEngine.Random.Range(0, DNAInstances.Count - 1);
        DNAInstances[currentDNAInstanceIndex].ShapedAgain();

        float[] values = new float[keepModifiers];
        DNAModifier[] mods = new DNAModifier[keepModifiers];
        float auxV;
        DNAModifier auxD;
        foreach (DNAModifier dM in this.DNAMods)
        {
            float v = dM.GetChange(DNAInstances[this.currentDNAInstanceIndex].GetDNAModifierValue(dM.GetDNAId()));
            DNAModifier d = dM;
            for (int i = keepModifiers -1; i >= 0; i--)
            {
                if (v < values[i])
                    continue;
                
                for (int q = i; q >= 0; q--)
                {
                    auxV = values[q];
                    auxD = mods[q];
                    values[q] = v;
                    mods[q] = d;
                    v = auxV;
                    d = auxD; 
                }
                if (d != null)
                    d.ResetCurrentValue();

                break;
            }
        }

        foreach(DNAModifier dM in mods)
        {
            if (dM == null)
                continue;

            dM.ChangeCurrentValue(DNAInstances[this.currentDNAInstanceIndex].GetDNAModifierValue(dM.GetDNAId()));
        }
        
        fluidM.ChangeEmittersColor(DNAInstances[this.currentDNAInstanceIndex].GetColor());

    }


    private void SaveDNAToSettingsXML()
    {
        XmlNodeList dnaInstancesDefinition = doc.SelectNodes("app_settings/DNAInstances/DNAInstance");

        for (int i = 0; i < dnaInstancesDefinition.Count; i++)
        {

            if (String.Compare(dnaInstancesDefinition[i].Attributes["Name"].Value, this.DNAInstances[currentDNAInstanceIndex].GetName()) == 0)
            {
                //we have the current DNAInstance

                dnaInstancesDefinition[i].Attributes["Amount"].Value = DNAInstances[currentDNAInstanceIndex].GetAmount() + "";

                XmlNodeList dnaModValues = dnaInstancesDefinition[i].SelectNodes("DNAModifierValue");

                for (int j = 0; j < dnaModValues.Count; j++){
                    string dnaModId = dnaModValues[j].Attributes["Id"].Value;
                    DNAModifier dnaMod = this.GetDNAModById(dnaModId);

                    if (dnaMod != null) {
                        //dnaModValues[j].Attributes["CurrentNormalizedValue"].Value = dnaMod.GetNormalizedValueForXMLStorage().ToString();
                        float value = dnaMod.GetValueForXMLStorage();
                        dnaModValues[j].Attributes["CurrentValue"].Value = dnaMod.GetValueForXMLStorage().ToString();
                    }
                }
            }
        }

        doc.Save(xmlPath);
       

    }

    #region XMLInteractionFunctions definition



    public void CreateInteractionsXML()
    {
        this.interactionsXML = new XmlDocument();
        string filePath = this.basePath + "\\interactions.xml";
        if (!File.Exists(filePath))
        {
            XmlDeclaration xmlDeclaration = interactionsXML.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlNode rootNode = interactionsXML.DocumentElement;
            interactionsXML.InsertBefore(xmlDeclaration, rootNode);

            XmlNode element = interactionsXML.CreateElement("Interactions");
            interactionsXML.AppendChild(element);
            interactionsXML.Save(filePath);
        }
        else
        {
            interactionsXML.Load(filePath);
        }

    }


	//Agrega un nodo Item que contiene un atributo id y dos nodos hijos(Time, Foto)
	public void AddInteractionXMLNode(string FileName){

		XmlNode interactionNode = interactionsXML.CreateElement("Interaction");

		XmlNode InteractionsRoot = interactionsXML.SelectSingleNode ("Interactions");

		XmlAttribute id = interactionsXML.CreateAttribute ("id");
		id.Value = DateTime.Now.Ticks.ToString ();
		interactionNode.Attributes.Append (id);

		XmlAttribute file = interactionsXML.CreateAttribute ("file");
		file.Value = FileName;
		interactionNode.Attributes.Append (file);

        XmlAttribute DNAInstance = interactionsXML.CreateAttribute("dnaInstance");
        DNAInstance.Value = this.DNAInstances[currentDNAInstanceIndex].GetName();
        interactionNode.Attributes.Append(DNAInstance);

        XmlAttribute InteractionDate = interactionsXML.CreateAttribute("date");
        InteractionDate.Value = DateTime.Now.ToString("MM-dd-yyyy");
        interactionNode.Attributes.Append(InteractionDate);

        XmlAttribute Recordings = interactionsXML.CreateAttribute("recordings");
        Recordings.Value = sender.GetFileName();
        interactionNode.Attributes.Append(Recordings);

        for (int i = 0; i < this.DNAMods.Count; i++) {
			XmlAttribute DNAModif = interactionsXML.CreateAttribute (this.DNAMods[i].GetDNAId() + "_Value");
			DNAModif.Value = this.DNAMods [i].GetValueForXMLStorage().ToString ();
			interactionNode.Attributes.Append (DNAModif);
		}

		InteractionsRoot.InsertAfter (interactionNode, InteractionsRoot.LastChild);

		string filePath = this.basePath + "\\interactions.xml";

		interactionsXML.Save (filePath);
	}

	#endregion

    private DNAModifier GetDNAModById(string Id)
    {
        DNAModifier returnedObject = null;
        bool found = false;
        int count = 0;
        while(!found && count < this.DNAMods.Count)
        {
            if (String.Compare(Id, this.DNAMods[count].GetDNAId()) == 0)
            {
                found = true;
                returnedObject = this.DNAMods[count];
            }
            else
            {
                count += 1;
            }
        }
        return returnedObject;
    }
    
    public int GetState()
    {
        return (int)currentState;
    }

    public void CalibrationFinished()
    {
        ChangeAppState(AppState.Shaping);
    }
    

    public void StartAllThings()
    {
        ChangeAppState(AppState.Calibrating);
    }

    public void StopAllThings()
    {
        ChangeAppState(AppState.Idle);
        sender.StopRecording();
        sender.ClearRecords();
    }
    
    public string GetCurrentConceptName()
    {
        return this.DNAInstances[currentDNAInstanceIndex].GetName();
    }

    public int GetCurrentDNAIndex()
    {
        return this.currentDNAModifierIndex;
    }
}
