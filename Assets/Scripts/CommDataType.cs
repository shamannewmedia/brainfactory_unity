﻿using UnityEngine;

[System.Serializable]
public class CommDataType
{
	public bool keepListening;
	public string inputDirectoryPath;
	public string inputFileName;
}