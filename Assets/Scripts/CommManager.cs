﻿// This is the client DLL class code to use for the sockServer
// include this DLL in your Plugins folder under Assets
// using it is very simple
// Look at LinkSyncSCR.cs
using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Collections;

namespace SharpConnect
{
	public class CommManager
	{
		const int READ_BUFFER_SIZE = 255;
		const int PORT_NUM = 10000;
		private TcpClient client = null;
		private byte[] readBuffer = new byte[READ_BUFFER_SIZE];
		public ArrayList lstUsers=new ArrayList();
		public string strMessage=string.Empty;
		public string res=String.Empty;
		//private string pUserName;

		public CommManager(){}

		public string fnConnectResult(string sNetIP, int iPORT_NUM)
		{
			try 
			{
				//pUserName=sUserName;
				// The TcpClient is a subclass of Socket, providing higher level 
				// functionality like streaming.
				client = new TcpClient(sNetIP, iPORT_NUM);
				// Start an asynchronous read invoking DoRead to avoid lagging the user
				// interface.
				client.GetStream().BeginRead(readBuffer, 0, READ_BUFFER_SIZE, new AsyncCallback(DoRead), null);
				// Make sure the window is showing before popping up connection dialog.

				//AttemptLogin(sUserName);
				return "Connection Succeeded";
			} 
			catch(Exception ex)
			{
				return "Server is not active.  Please start server and try again.      " + ex.ToString();
			}
		}
			

		private void DoRead(IAsyncResult ar)
		{ 
			int BytesRead;
			try
			{
				// Finish asynchronous read into readBuffer and return number of bytes read.
				BytesRead = client.GetStream().EndRead(ar);
				if (BytesRead < 1) 
				{
					// if no bytes were read server has close.  
					res="[[Disconnected]]";
					return;
				}
				// Convert the byte array the message was saved into, minus two for the
				// Chr(13) and Chr(10)
				strMessage = Encoding.ASCII.GetString(readBuffer, 0, BytesRead - 2);
				//ProcessCommands(strMessage);
				// Start a new asynchronous read into readBuffer.
				client.GetStream().BeginRead(readBuffer, 0, READ_BUFFER_SIZE, new AsyncCallback(DoRead), null);

			} 
			catch
			{
				res="[[Disconnected]]";
			}
		}

		// Use a StreamWriter to send a message to server.
		public void SendData(string data)
		{
			StreamWriter writer = new StreamWriter(client.GetStream());
			writer.Write(data + "[/TCP]");
			writer.Flush();
		}

		public void CloseConnection(){
			client.Close();
			client = null;
		}

		public bool IsConnected(){
			return this.client.Connected;
		}
	}
}