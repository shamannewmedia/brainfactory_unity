﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml;


[System.Serializable]
public class DNAInstance  {

    Dictionary<string, float> currentDNAValues;
    string name;

    private Color32 color;
    private string koreanName;
    private int amount = 0;


    public DNAInstance(string _name, string koreanName, int amount)
    {
        this.name = _name;
        this.koreanName = koreanName;
        this.amount = amount;

        this.currentDNAValues = new Dictionary<string, float>();
    }

    public DNAInstance(XmlNode info)
    {
        this.name = info.Attributes["Name"].Value; ;
        this.currentDNAValues = new Dictionary<string, float>();

        XmlNodeList dnaModValues = info.SelectNodes("DNAModifierValue");

        if (info.Attributes["Color"] != null)
            color = Utils.hexToColor(info.Attributes["Color"].Value);

        if (info.Attributes["Korean"] != null)
            koreanName = info.Attributes["Korean"].Value;

        if (info.Attributes["Amount"] != null)
            amount = int.Parse(info.Attributes["Amount"].Value);

        for (int i = 0; i < dnaModValues.Count; i++)
        {
            string id = dnaModValues[i].Attributes["Id"].Value;
            //float value = float.Parse(dnaModValues[i].Attributes["CurrentNormalizedValue"].Value);
            float value = float.Parse(dnaModValues[i].Attributes["CurrentValue"].Value);
            this.AddDNAModifier(id, value);
        }
    }

    public void AddDNAModifier (string dnaModID, float currentNormalizedValue)
    {
        this.currentDNAValues.Add(dnaModID, currentNormalizedValue);
    }

    public float GetDNAModifierValue (string dnaModID)
    {
        if (this.currentDNAValues.ContainsKey(dnaModID))
        {
            return this.currentDNAValues[dnaModID];
        }
        else
        {
            throw new System.Exception("DNA ID not found in DNAInstance.");
        }
    }

    public void EditDNAModifierValue(string dnaModID, float newNormalizedValue)
    {
        if (this.currentDNAValues.ContainsKey(dnaModID))
        {
            this.currentDNAValues[dnaModID] = newNormalizedValue;
        }
    }

    public string  GetName() { return this.name; }

    public string GetKoreanName() { return koreanName; }

    public Color32 GetColor() { return color; }

    public void ShapedAgain() { amount++;  }
    public int GetAmount() { return amount; }
}
