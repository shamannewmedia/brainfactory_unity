﻿using UnityEngine;
using System;
using System.Xml;
using System.Xml.Serialization;
using Vatio.Filters;
using Rug.Osc;

[System.Serializable]
public abstract class DNAModifier {

	public enum DNATypeDef {EllipsoidEff, PointEff, Fluid, Emitter, System};

	protected string DNAId;
	protected string TextDescription;
	protected DNATypeDef DNAType;

    protected float baseValue;
    protected float currentValue;
	public float CurrentNormalizedValue;
	public float MinRealValue;
	public float MaxRealValue;

	public WrapMode wrapMode = WrapMode.Clamp;

	public float alpha = 0.05f;
	protected LowPassFilter<float> lPassFilter;

    public int direction = 1; // 1, 0, -1
    public float modificationSpeed = 0.1f;
    protected float time;
    protected bool modifying = false;

    public float timeToOk = 1.0f;
    protected float okTime = 0f;

    private bool bounce = false;

    private string effectorName = "";

	public DNAModifier(string id)
	{
		this.DNAId = id;
	}

	public DNAModifier(XmlNode info)
	{
        time = GameObject.Find("Manager").GetComponent<AppManager>().shapingWindowTime;

		this.DNAId = info.Attributes["Id"].Value;
		this.TextDescription = info.Attributes ["TextDescription"].Value;
		this.CurrentNormalizedValue = 0.0f; //this fielf will get initialized on each iteration by the value held in the DNA Instance.
		this.MinRealValue = float.Parse(info.Attributes ["MinRealValue"].Value);
		this.MaxRealValue = float.Parse(info.Attributes ["MaxRealValue"].Value);


        if (info.Attributes["Speed"] != null)
            modificationSpeed = float.Parse(info.Attributes["Speed"].Value);
        else
            modificationSpeed = (MaxRealValue - MinRealValue) / time;
        
    
        if (info.Attributes["Base"] != null)
            baseValue = float.Parse(info.Attributes["Base"].Value);
        else
            baseValue = MinRealValue;

        if (info.Attributes["Bounce"] != null)
        {
            bounce = info.Attributes["Bounce"].Value.ToLower().Equals("true") ? true : false;
        }

        if (info.Attributes["EffectorIdentifier"] != null)
            effectorName = info.Attributes["EffectorIdentifier"].Value;

        currentValue = baseValue;
		initializeLowPassFilter ();
	}

	public string GetDNAId(){
		return this.DNAId;
	}

    protected abstract void SetModifierValue();

    public void ChangeCurrentValue(float value)
    {
        currentValue = ReachedLimits(value);
        SetModifierValue();
    }

    public void ResetCurrentValue()
    {
        currentValue = baseValue;
        SetModifierValue();
    }

	//this method should set the CurrentNormalizedValue, and alter all specific parameter of objects of every specific class that extends this abstract class.
	public abstract void SetNormalizedValue(float newVal, bool useLowPassFilter);

    public void SetValue(float newVal)
    {
        direction = (int)newVal;
    }

    public float GetValueForXMLStorage()
    {
        return currentValue;
    }

    public abstract float GetNormalizedValueForXMLStorage();
    
    public abstract void Modify();

    public float GetCurrentNormalizedValue (){
		//this value should be the only value stored in the class.
		return this.CurrentNormalizedValue;
	}

    public float GetCurrentValue()
    {
        return currentValue;
    }

	public float GetCurrentRealValue (){
		return this.Map(CurrentNormalizedValue, 0.0f, 1.0f, MinRealValue, MaxRealValue);
	}

	public float GetMinRealValue(){
		return this.MinRealValue;
	}

	public float GetMaxRealValue(){
		return this.MaxRealValue;
	}

	public DNATypeDef GetDNAModifierType(){
		return this.DNAType;
	}

	protected float Map(float x, float in_min, float in_max, float out_min, float out_max)
	{
		return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}

	protected float WrapValue( float v, float start, float end, WrapMode wMode )
	{
		switch( wMode )
		{
		case WrapMode.Clamp:
		case WrapMode.ClampForever:
			return Mathf.Clamp( v, start, end );
		case WrapMode.Default:
		case WrapMode.Loop:
			return Mathf.Repeat( v, end - start ) + start;
		case WrapMode.PingPong:
			return Mathf.PingPong( v, end - start ) + start;
		default:
			return v;
		}
	}

	protected void initializeLowPassFilter (){
		//we initialize with the current value.
		float ini = CurrentNormalizedValue;
		this.lPassFilter = new LowPassFilter<float> (alpha, ini);
	}

    protected float ReachedLimits(float value)
    {
        float ret = value;

        if (value > MaxRealValue)
        {
            ret = MaxRealValue;
            if (bounce) direction *= -1;
            else        direction = 0;
        }

        if (value < MinRealValue)
        {
            ret = MinRealValue;
            if (bounce) direction *= -1;
            else        direction = 0;
        }

        return ret;
    }

    public float GetChange(float value)
    {
        float v = Math.Abs(baseValue - value);
        return v / (MaxRealValue - MinRealValue);
    }

    public string GetEffectorName()
    {
        return effectorName;
    }
}
