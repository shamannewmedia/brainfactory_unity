﻿using UnityEngine;
using System.Collections;
using Thinksquirrel.Fluvio;
using Thinksquirrel.Fluvio.Plugins.Effectors;
using Vatio.Filters;
using System;
using System.Xml;
using System.Xml.Serialization;
using WhiteCat.Paths;

public class DNAModifierEllipsoid : DNAModifier {

	public enum EllipsoidModType {ForceAmount, Vorticity, Position, Range};

	private FluidEllipsoidEffector Effector;

	public EllipsoidModType modType;

	private BezierPath spline;

    private float splineDistance;
    Location location = new Location();
    [SerializeField]
    float speed = 1f;


    public DNAModifierEllipsoid (XmlNode info)
		: base(info)//Call base constructor explicitly
	{

		AppManager aM = GameObject.Find ("Manager").GetComponent<AppManager> ();

		// getting effector

		string effectorId = info.Attributes ["EffectorIdentifier"].Value;
        
        Effector = GameObject.Find(effectorId).GetComponent<FluidEllipsoidEffector>();

        //getting spline

        if (info.Attributes["SplineId"] != null)
        {
            int splineId = int.Parse(info.Attributes["SplineId"].Value);
            this.spline = aM.GetSplineByIdentifier(splineId);
            this.splineDistance = 0.0f;
        }

		//setting effector type

		this.DNAType = DNATypeDef.EllipsoidEff;

		//setting modType


		string myModType = info.Attributes ["Mode"].Value;

		switch (myModType)
		{
		    case "Force":
			    this.modType = EllipsoidModType.ForceAmount;
                baseValue = Effector.force.maxConstant;
			    break;

		    case "Position":
			    this.modType = EllipsoidModType.Position;
                //setting up initial position from normalized index
                float totalLength = spline.length;
                float currentLengthPosition = totalLength * baseValue;
                SetSplinePositionByLength(currentLengthPosition);
                baseValue = currentLengthPosition;
                MinRealValue = 0;
                MaxRealValue = spline.length;
                break;

		    case "Vorticity":
			    this.modType = EllipsoidModType.Vorticity;
                baseValue = Effector.vorticity.maxConstant;
			    break;

            case "Range":
                modType = EllipsoidModType.Range;
                baseValue = Effector.effectorRange;
                break;
		}

        currentValue = baseValue;
        SetModifierValue();
	}

    protected override void SetModifierValue()
    {
        switch (modType)
        {
            case EllipsoidModType.ForceAmount:
                Effector.force.maxConstant = currentValue;
                Effector.force.minConstant = currentValue;
                break;

            case EllipsoidModType.Vorticity:
                Effector.vorticity.maxConstant = currentValue;
                Effector.vorticity.minConstant = currentValue;
                break;

            case EllipsoidModType.Range:
                Effector.effectorRange = currentValue;
                break;

            case EllipsoidModType.Position:
                if (this.spline)
                {
                    if (!spline.circular) this.currentValue = Mathf.Clamp(this.currentValue, 0f, this.spline.length);

                    location = spline.GetLocationByLength(currentValue, location.index);
                    this.Effector.worldPosition = spline.GetPoint(location);
                }

                break;
        }
    }

    public override void Modify()
    {
        float value = Time.deltaTime * modificationSpeed * direction;
        currentValue += value;
        switch (modType)
        {
            case EllipsoidModType.ForceAmount:
                currentValue = ReachedLimits(currentValue);

                Effector.force.maxConstant = currentValue;
                Effector.force.minConstant = currentValue;

                //Effector.force.maxConstant += value;
                //Effector.force.maxConstant = ReachedLimits(Effector.force.maxConstant);

                //Effector.force.minConstant = Effector.force.maxConstant;
                break;

            case EllipsoidModType.Vorticity:

                currentValue = ReachedLimits(currentValue);
                Effector.vorticity.maxConstant = currentValue;
                Effector.vorticity.minConstant = currentValue;

                //Effector.vorticity.maxConstant += value;
                //Effector.vorticity.maxConstant = ReachedLimits(Effector.vorticity.maxConstant);

                //Effector.vorticity.minConstant = Effector.force.maxConstant;
                break;

            case EllipsoidModType.Range:
                currentValue = ReachedLimits(currentValue);

                Effector.effectorRange = currentValue;

                //Effector.effectorRange += value;
                //Effector.effectorRange = ReachedLimits(Effector.effectorRange);
                break;

            case EllipsoidModType.Position:

                splineDistance += direction * Time.deltaTime * speed;
                currentValue = splineDistance;

                if (this.spline)
                {
                    if (!spline.circular) this.splineDistance = Mathf.Clamp(this.splineDistance, 0f, this.spline.length);

                    location = spline.GetLocationByLength(splineDistance, location.index);
                    this.Effector.worldPosition = spline.GetPoint(location);
                }

                break;
        }
    }

    public override void SetNormalizedValue (float newVal, bool useLowPassFilter)
    {
        if (newVal > 0.4 && newVal < 0.6)
        {
            direction = 0;
            okTime += Time.deltaTime;
            if (okTime >= timeToOk)
                GameObject.Find("Manager").GetComponent<AppManager>().ModifierDone();

            return;
        }

        okTime = 0.0f;
        direction = newVal > 0.5 ? 1 : -1;
    }

    public void SetSplinePositionByLength(float currentLength)
    {
        if (this.spline)
        {
            if (!spline.circular) this.splineDistance = Mathf.Clamp(this.splineDistance, 0f, this.spline.length);

            location = spline.GetLocationByLength(currentLength);
            this.Effector.worldPosition = spline.GetPoint(location);
        }
    }

    public override float GetNormalizedValueForXMLStorage()
    {
        float retVal = 0.0f;
        switch (modType)
        {
            case EllipsoidModType.ForceAmount:

                retVal = this.GetCurrentNormalizedValue();
                break;
            case EllipsoidModType.Vorticity:

                retVal = this.GetCurrentNormalizedValue();

                break;
            case EllipsoidModType.Position:

                float totalLength = spline.length;
                float currentLength = spline.GetLength(location);

                retVal = currentLength / totalLength;
                
                break;
        }

        return retVal;
    }


}
