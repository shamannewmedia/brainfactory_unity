﻿using UnityEngine;
using System.Collections;
using WhiteCat.Paths;
using System.Xml;
using System;

public class DNAModifierEmitter : DNAModifier
{
    public enum EmitterModType { Speed, Repeaters, Position, EmitterNumber, DirectionX, DirectionY, DirectionZ, Color };
    private EmitterModType modType;

    private BezierPath spline;
    private float splineDistance;
    Location location = new Location();
    [SerializeField]
    float speed = 1f;

    private GameObject Emitter;

    private float acumulatedValue = 0;

    public DNAModifierEmitter (XmlNode info) : base(info)
    {
        DNAType = DNATypeDef.Emitter;
        
        AppManager aM = GameObject.Find("Manager").GetComponent<AppManager>();

        if (info.Attributes["SplineId"] != null)
        {
            int splineId = int.Parse(info.Attributes["SplineId"].Value);
            this.spline = aM.GetSplineByIdentifier(splineId);
            this.splineDistance = 0.0f;
        }

        if (modType != EmitterModType.EmitterNumber)
        {
            string emitterId = info.Attributes["EmitterIdentifier"].Value;
            Emitter = GameObject.Find(emitterId);
        }

        string myModType = info.Attributes["Mode"].Value;
        switch (myModType)
        {
            case "Speed":
                this.modType = EmitterModType.Speed;
                baseValue = Emitter.GetComponent<emitter>().speed;
                break;

            case "Position":
                this.modType = EmitterModType.Position;
                //setting up initial position from normalized index
                float totalLength = spline.length;
                float currentLengthPosition = totalLength * baseValue;
                SetSplinePositionByLength(currentLengthPosition);
                baseValue = currentLengthPosition;
                MinRealValue = 0;
                MaxRealValue = spline.length;
                break;

            case "Repeaters":
                this.modType = EmitterModType.Repeaters;
                baseValue = Emitter.GetComponent<emitter>().repeaters;
                break;

            case "EmitterNumber":
                modType = EmitterModType.EmitterNumber;
                break;

            case "DirectionX":
                modType = EmitterModType.DirectionX;
                baseValue = Emitter.GetComponent<emitter>().direction.x;
                break;

            case "DirectionY":
                modType = EmitterModType.DirectionY;
                baseValue = Emitter.GetComponent<emitter>().direction.y;
                break;
            case "DirectionZ":
                modType = EmitterModType.DirectionZ;
                baseValue = Emitter.GetComponent<emitter>().direction.z;
                break;
        }

        currentValue = baseValue;
        SetModifierValue();
    }

    protected override void SetModifierValue()
    {
        Vector3 dir;
        switch (modType)
        {
            case EmitterModType.Position:
                SetSplinePositionByLength(currentValue);
                break;

            case EmitterModType.Repeaters:
                Emitter.GetComponent<emitter>().repeaters = (int)currentValue;
                break;

            case EmitterModType.Speed:
                Emitter.GetComponent<emitter>().speed = currentValue;
                break;

            case EmitterModType.EmitterNumber:
                GameObject.Find("Manager").GetComponent<FluidManager>().ChangeEmittersNumbers((int)currentValue);
                break;

            case EmitterModType.DirectionX:
                dir = Emitter.GetComponent<emitter>().direction;
                dir.x = currentValue;
                Emitter.GetComponent<emitter>().direction = dir;
                break;

            case EmitterModType.DirectionY:
                dir = Emitter.GetComponent<emitter>().direction;
                dir.y = currentValue;
                Emitter.GetComponent<emitter>().direction = dir;
                break;

            case EmitterModType.DirectionZ:
                dir = Emitter.GetComponent<emitter>().direction;
                dir.z = currentValue;
                Emitter.GetComponent<emitter>().direction = dir;
                break;
        }
    }

    public override void Modify()
    {
        Vector3 dir;
        float value = Time.deltaTime * modificationSpeed * direction;
        switch (modType)
        {
            case EmitterModType.Position:
                splineDistance += Time.deltaTime * speed * direction;
                currentValue = splineDistance;

                SetSplinePositionByLength(splineDistance);
                break;

            case EmitterModType.Repeaters:
                acumulatedValue += value;
                if (Mathf.Abs(acumulatedValue) < 1)
                    return;

                int repeaters = Emitter.GetComponent<emitter>().repeaters;
                repeaters += direction;

                currentValue = ReachedLimits((float)repeaters);
                Emitter.GetComponent<emitter>().repeaters = (int)currentValue;                

                acumulatedValue = 0;
                break;

            case EmitterModType.Speed:
                float splineSpeed = Emitter.GetComponent<emitter>().speed + value;
                Emitter.GetComponent<emitter>().speed = ReachedLimits(splineSpeed);
                break;

            case EmitterModType.EmitterNumber:
                acumulatedValue += value;
                if (Mathf.Abs(acumulatedValue) < 1)
                    return;

                int emitters = GameObject.Find("Manager").GetComponent<FluidManager>().GetEmittersNumber();
                emitters += direction;

                currentValue = ReachedLimits((float)emitters);
                GameObject.Find("Manager").GetComponent<FluidManager>().ChangeEmittersNumbers((int)currentValue);

                acumulatedValue = 0;              
                
                break;

            case EmitterModType.DirectionX:
                dir = Emitter.GetComponent<emitter>().direction;
                dir.x += value;
                currentValue = ReachedLimits(dir.x);
                dir.x = currentValue;
                Emitter.GetComponent<emitter>().direction = dir;
                break;

            case EmitterModType.DirectionY:
                dir = Emitter.GetComponent<emitter>().direction;
                dir.y += value;
                currentValue = ReachedLimits(dir.y);
                dir.y = currentValue;
                Emitter.GetComponent<emitter>().direction = dir;
                break;

            case EmitterModType.DirectionZ:
                dir = Emitter.GetComponent<emitter>().direction;
                dir.z += value;
                currentValue = ReachedLimits(dir.z);
                dir.z = currentValue;
                Emitter.GetComponent<emitter>().direction = dir;
                break;
        }
    }

    public override void SetNormalizedValue(float newVal, bool useLowPassFilter)
    {
        if (newVal > 0.4 && newVal < 0.6)
        {
            direction = 0;
            okTime += Time.deltaTime;
            if (okTime >= timeToOk)
                GameObject.Find("Manager").GetComponent<AppManager>().ModifierDone();

            return;
        }

        okTime = 0.0f;
        direction = newVal > 0.5 ? 1 : -1;
    }


    public override float GetNormalizedValueForXMLStorage()
    {
        if (modType != EmitterModType.Position)
            return GetCurrentNormalizedValue();

        float totalLength = spline.length;
        float currentLength = spline.GetLength(location);

        return currentLength / totalLength;
    }

    public void SetSplinePositionByLength(float currentLength)
    {
        if (this.spline)
        {
            if (!spline.circular) this.splineDistance = Mathf.Clamp(this.splineDistance, 0f, this.spline.length);

            location = spline.GetLocationByLength(currentLength);
            Emitter.transform.position = spline.GetPoint(location);
        }
    }
}
