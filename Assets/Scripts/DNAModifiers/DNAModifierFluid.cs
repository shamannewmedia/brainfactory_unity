﻿using UnityEngine;
using System.Collections;
using Thinksquirrel.Fluvio;
using System;
using System.Xml;
using System.Xml.Serialization;
using Vatio.Filters;
using WhiteCat.Paths;

public class DNAModifierFluid : DNAModifier {

	public enum FluidModType {Viscosity, Density, Throughput, GasConstant};

	public string fId; // fluid Id

	private FluidParticleSystem fluid;

	public FluidModType modType;

	private BezierPath spline;

	private float splineDistance;
    Location location = new Location();
    [SerializeField] float speed = 1f;

    public DNAModifierFluid (XmlNode info) : base(info)//Call base constructor explicitly
	{

		AppManager aM = GameObject.Find ("Manager").GetComponent<AppManager> ();

		// getting fluid
        this.fluid = aM.GetFluid();
        
		this.DNAType = DNATypeDef.Fluid;

		//setting modType
        

		string myModType = info.Attributes ["Mode"].Value;

		switch (myModType)
		{
		    case "Viscosity":
			    this.modType = FluidModType.Viscosity;
                baseValue = fluid.viscosity;
			    break;

		    case "Density":
			    this.modType = FluidModType.Density;
                baseValue = fluid.density;
			    break;

		    case "Throughput":
			    this.modType = FluidModType.Throughput;
                baseValue = GameObject.Find("Manager").GetComponent<FluidManager>().GetThroughput();

                break;

            case "GasConstant":
                this.modType = FluidModType.GasConstant;
                baseValue = fluid.gasConstant;
                break;

        }

        currentValue = baseValue;
        SetModifierValue();
    }

    protected override void SetModifierValue()
    {
        switch (modType)
        {
            case FluidModType.Density:
                fluid.density = currentValue;
                fluid.minimumDensity = currentValue;
                break;

            case FluidModType.Viscosity:
                fluid.viscosity = currentValue;
                break;

            case FluidModType.GasConstant:
                fluid.gasConstant = currentValue;
                break;

            case FluidModType.Throughput:
                FluidManager fm = GameObject.Find("Manager").GetComponent<FluidManager>();
                fm.ChangeThroughput((int)currentValue);
                break;
        }
    }

    public override void Modify()
    {
        float value = Time.deltaTime * modificationSpeed * direction;
        currentValue += value;
        currentValue = ReachedLimits(currentValue);
        switch (modType)
        {
            case FluidModType.Density:
                fluid.density = currentValue;
                fluid.minimumDensity = currentValue;

                //fluid.density += value;
                //fluid.density = ReachedLimits(fluid.density);

                //fluid.minimumDensity = fluid.density;
                break;

            case FluidModType.Viscosity:
                fluid.viscosity = currentValue;

                //fluid.viscosity += value;
                //fluid.viscosity = ReachedLimits(fluid.viscosity);
                break;

            case FluidModType.GasConstant:
                fluid.gasConstant = currentValue;

                //fluid.gasConstant += value;
                //fluid.gasConstant = ReachedLimits(fluid.gasConstant);
                break;

            case FluidModType.Throughput:
                FluidManager fm = GameObject.Find("Manager").GetComponent<FluidManager>();
                //int throughput = fm.GetThroughput();
                //throughput += (int)value;
                //currentValue = ReachedLimits((float)throughput);
                fm.ChangeThroughput((int)currentValue);
                break;
        }
    }

    public override void SetNormalizedValue (float newVal, bool useLowPassFilter)
    {
        if (newVal > 0.4 && newVal < 0.6)
        {
            direction = 0;
            okTime += Time.deltaTime;
            if (okTime >= timeToOk)
                GameObject.Find("Manager").GetComponent<AppManager>().ModifierDone();

            return;
        }

        okTime = 0.0f;
        direction = newVal > 0.5 ? 1 : -1;
    }

	public FluidModType GetCurrentModType(){
		return modType;
	}

	public void SetCurrentModType(FluidModType newType){
		this.modType = newType;
	}

    public override float GetNormalizedValueForXMLStorage()
    {
        float retVal = 0.0f;
        
        retVal = this.GetCurrentNormalizedValue();

        return retVal;
    }

}
