﻿using UnityEngine;
using System.Collections;
using Thinksquirrel.Fluvio;
using Thinksquirrel.Fluvio.Plugins.Effectors;
using Vatio.Filters;
using System;
using System.Xml;
using System.Xml.Serialization;
using WhiteCat.Paths;

public class DNAModifierPoint : DNAModifier {

	public enum PointModType {ForceAmount, Vorticity, Position, Range};

    private FluidPointEffector Effector;

	public PointModType modType;

    private BezierPath spline;

    private float splineDistance;
    Location location = new Location();
    [SerializeField]
    float speed = 1f;

    public DNAModifierPoint (XmlNode info) : base(info)//Call base constructor explicitly
	{

        //setting effector type
        this.DNAType = DNATypeDef.PointEff;

        AppManager aM = GameObject.Find ("Manager").GetComponent<AppManager> ();

		// getting effector
		string effectorId = info.Attributes ["EffectorIdentifier"].Value;
        
        Effector = GameObject.Find(effectorId).GetComponent<FluidPointEffector>();
        
        //getting spline
        if (info.Attributes["SplineId"] != null)
        {
            int splineId = int.Parse(info.Attributes["SplineId"].Value);
            this.spline = aM.GetSplineByIdentifier(splineId);
            this.splineDistance = 0.0f;
        }

        //setting modType
        string myModType = info.Attributes ["Mode"].Value;

		switch (myModType)
		{
		    case "Force":
			    this.modType = PointModType.ForceAmount;
                baseValue = Effector.force.maxConstant;
			    break;

		    case "Position":
			    this.modType = PointModType.Position;
                float totalLength = spline.length;
                float currentLengthPosition = totalLength * baseValue;
                SetSplinePositionByLength(currentLengthPosition);
                baseValue = currentLengthPosition;
                MinRealValue = 0;
                MaxRealValue = spline.length;
                break;

		    case "Vorticity":
			    this.modType = PointModType.Vorticity;
                baseValue = Effector.vorticity.maxConstant;
			    break;

            case "Range":
                modType = PointModType.Range;
                baseValue = Effector.effectorRange;
                break;
		}
        
        currentValue = baseValue;
        SetModifierValue();
    }

    protected override void SetModifierValue()
    {
        switch (modType)
        {
            case PointModType.ForceAmount:
                Effector.force.maxConstant = currentValue;
                Effector.force.minConstant = currentValue;
                break;

            case PointModType.Vorticity:
                Effector.vorticity.maxConstant = currentValue;
                Effector.vorticity.minConstant = currentValue;
                break;

            case PointModType.Range:
                Effector.effectorRange = currentValue;
                break;

            case PointModType.Position:
                if (this.spline)
                {
                    if (!spline.circular) this.currentValue = Mathf.Clamp(this.currentValue, 0f, this.spline.length);

                    location = spline.GetLocationByLength(currentValue, location.index);
                    this.Effector.worldPosition = spline.GetPoint(location);
                }

                break;
        }
    }

    public override void Modify()
    {
        float value = Time.deltaTime * modificationSpeed * direction;
        currentValue += value;
        switch (modType)
        {
            case PointModType.ForceAmount:
                currentValue = ReachedLimits(currentValue);

                Effector.force.maxConstant = currentValue;
                Effector.force.minConstant = currentValue;
                break;

            case PointModType.Vorticity:
                currentValue = ReachedLimits(currentValue);

                Effector.vorticity.maxConstant = currentValue;
                Effector.vorticity.minConstant = currentValue;
                break;

            case PointModType.Range:
                currentValue = ReachedLimits(currentValue);

                Effector.effectorRange = currentValue;
                break;

            case PointModType.Position:

                splineDistance += direction * Time.deltaTime * speed;
                currentValue = splineDistance;

                if (this.spline)
                {
                    if (!spline.circular) this.splineDistance = Mathf.Clamp(this.splineDistance, 0f, this.spline.length);

                    location = spline.GetLocationByLength(splineDistance, location.index);
                    this.Effector.worldPosition = spline.GetPoint(location);
                }

                break;
        }
    }

    public override void SetNormalizedValue (float newVal, bool useLowPassFilter)
	{
        if (newVal > 0.4 && newVal < 0.6)
        {
            direction = 0;
            okTime += Time.deltaTime;
            if (okTime >= timeToOk)
                GameObject.Find("Manager").GetComponent<AppManager>().ModifierDone();
            
            return;
        }

        okTime = 0.0f;
        direction = newVal > 0.5 ? 1 : -1;
    }

	public PointModType GetCurrentModType(){
		return modType;
	}

	public void SetCurrentModType(PointModType newType){
		this.modType = newType;
	}

    public override float GetNormalizedValueForXMLStorage()
    {
        float retVal = 0.0f;
        switch (modType)
        {
            case PointModType.ForceAmount:
                retVal = this.GetCurrentNormalizedValue();
                break;

            case PointModType.Vorticity:
                retVal = this.GetCurrentNormalizedValue();
                break;

            case PointModType.Range:
                retVal = this.GetCurrentNormalizedValue();
                break;

            case PointModType.Position:

                float totalLength = spline.length;
                float currentLength = spline.GetLength(location);

                retVal = currentLength / totalLength;

                break;
        }

        return retVal;
    }

    public void SetSplinePositionByLength(float currentLength)
    {
        if (this.spline)
        {
            if (!spline.circular) this.splineDistance = Mathf.Clamp(this.splineDistance, 0f, this.spline.length);

            location = spline.GetLocationByLength(currentLength);
            this.Effector.worldPosition = spline.GetPoint(location);
        }
    }

}

