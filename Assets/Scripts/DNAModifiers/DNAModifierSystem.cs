﻿using UnityEngine;
using System.Collections;
using Thinksquirrel.Fluvio;
using Thinksquirrel.Fluvio.Plugins.Effectors;
using System.Xml;
using System;
using WhiteCat.Paths;

public class DNAModifierSystem : DNAModifier {

    public enum SystemModType { ForceAmount, Vorticity, Position, Scale, Range, RotationX, RotationY, RotationZ };
    public SystemModType modType;

    private GameObject system;

    private BezierPath spline;
    private float splineDistance;
    Location location = new Location();
    [SerializeField]
    float positionSpeed = 1;

    [SerializeField]
    float rotationSpeed = 1;

    private float angleX = 0f, angleY = 0f, angleZ = 0;
    private float acumulatedValue = 1f;


    public DNAModifierSystem (XmlNode info)
        :base(info)
    {
        AppManager aM = GameObject.Find("Manager").GetComponent<AppManager>();

        string systemId = info.Attributes["EffectorIdentifier"].Value;
        system = GameObject.Find(systemId);

        if (info.Attributes["SplineId"] != null)
        {
            int splineId = int.Parse(info.Attributes["SplineId"].Value);
            this.spline = aM.GetSplineByIdentifier(splineId);
            this.splineDistance = 0.0f;
        }

        DNAType = DNATypeDef.System;

        string myModType = info.Attributes["Mode"].Value;
        switch (myModType)
        {
            case "Force":
                modType = SystemModType.ForceAmount;
                baseValue = 1;
                break;
            case "Vorticity":
                modType = SystemModType.Vorticity;
                baseValue = 1;
                break;
            case "Position":
                modType = SystemModType.Position;
                float totalLength = spline.length;
                float currentLengthPosition = totalLength * baseValue;
                SetSplinePositionByLength(currentLengthPosition);
                baseValue = currentLengthPosition;
                MinRealValue = 0;
                MaxRealValue = spline.length;
                break;
            case "Scale":
                modType = SystemModType.Scale;
                baseValue = 1;
                break;
            case "Range":
                modType = SystemModType.Range;
                baseValue = 1;
                break;
            case "RotationX":
                modType = SystemModType.RotationX;
                baseValue = system.transform.rotation.eulerAngles.x;
                break;
            case "RotationY":
                modType = SystemModType.RotationY;
                baseValue = system.transform.rotation.eulerAngles.y;
                break;
            case "RotationZ":
                modType = SystemModType.RotationZ;
                baseValue = system.transform.rotation.eulerAngles.z;
                break;
        }

        currentValue = baseValue;
        SetModifierValue();
    }

    protected override void SetModifierValue()
    {
        switch (modType)
        {
            case SystemModType.ForceAmount:
                for (int i = 0; i < system.transform.childCount; i++)
                {
                    GameObject go = system.transform.GetChild(i).gameObject;
                    FluidEffector effector = go.GetComponent<FluidEffector>();
                    effector.force.maxConstant = system.GetComponent<SystemProperties>().forces[go.name] * currentValue;
                    effector.force.minConstant = effector.force.maxConstant;
                }
                break;

            case SystemModType.Vorticity:
                for (int i = 0; i < system.transform.childCount; i++)
                {
                    GameObject go = system.transform.GetChild(i).gameObject;
                    FluidEffector effector = go.GetComponent<FluidEffector>();
                    effector.vorticity.maxConstant = system.GetComponent<SystemProperties>().vorticity[go.name] * currentValue;
                    effector.vorticity.minConstant = effector.vorticity.maxConstant;
                }
                break;

            case SystemModType.Range:
                for (int i = 0; i < system.transform.childCount; i++)
                {
                    GameObject go = system.transform.GetChild(i).gameObject;
                    FluidEffector effector = go.GetComponent<FluidEffector>();
                    effector.effectorRange = system.GetComponent<SystemProperties>().range[go.name] * currentValue;
                }
                break;

            case SystemModType.Scale:
                system.transform.localScale = new Vector3(currentValue, currentValue, currentValue);
                break;

            case SystemModType.Position:
                if (this.spline)
                {
                    if (!spline.circular) this.currentValue = Mathf.Clamp(this.currentValue, 0f, this.spline.length);

                    location = spline.GetLocationByLength(currentValue, location.index);
                    system.transform.position = spline.GetPoint(location);
                }

                break;

            case SystemModType.RotationX:
                system.transform.rotation = Quaternion.Euler(currentValue, system.transform.rotation.eulerAngles.y, system.transform.rotation.eulerAngles.z);
                break;

            case SystemModType.RotationY:
                system.transform.rotation = Quaternion.Euler(system.transform.rotation.eulerAngles.x, currentValue, system.transform.rotation.eulerAngles.z);
                break;

            case SystemModType.RotationZ:
                system.transform.rotation = Quaternion.Euler(system.transform.rotation.eulerAngles.x, system.transform.rotation.eulerAngles.y, currentValue);
                break;
        }
    }

    public override void Modify()
    {
        float value = Time.deltaTime * modificationSpeed * direction;
        acumulatedValue += value;
        currentValue += value;
        switch (modType)
        {
            case SystemModType.ForceAmount:
                currentValue = ReachedLimits(currentValue);
                for (int i = 0; i < system.transform.childCount; i++)
                {
                    GameObject go = system.transform.GetChild(i).gameObject;
                    FluidEffector effector = go.GetComponent<FluidEffector>();
                    effector.force.maxConstant = system.GetComponent<SystemProperties>().forces[go.name] * currentValue;
                    effector.force.minConstant = effector.force.maxConstant;
                }
                break;

            case SystemModType.Vorticity:
                currentValue = ReachedLimits(currentValue);
                for (int i = 0; i < system.transform.childCount; i++)
                {
                    GameObject go = system.transform.GetChild(i).gameObject;
                    FluidEffector effector = go.GetComponent<FluidEffector>();
                    effector.vorticity.maxConstant = system.GetComponent<SystemProperties>().vorticity[go.name] * currentValue;
                    effector.vorticity.minConstant = effector.vorticity.maxConstant;
                }
                break;

            case SystemModType.Range:
                currentValue = ReachedLimits(currentValue);
                for (int i = 0; i < system.transform.childCount; i++)
                {
                    GameObject go = system.transform.GetChild(i).gameObject;
                    FluidEffector effector = go.GetComponent<FluidEffector>();
                    effector.effectorRange = system.GetComponent<SystemProperties>().range[go.name] * currentValue;
                }
                break;

            case SystemModType.Scale:
                currentValue = ReachedLimits(currentValue);
                system.transform.localScale = new Vector3(currentValue, currentValue, currentValue);
                break;

            case SystemModType.Position:
                this.splineDistance += Time.deltaTime * this.positionSpeed * direction;
                currentValue = splineDistance;
                if (this.spline)
                {
                    if (!spline.circular) this.splineDistance = Mathf.Clamp(this.splineDistance, 0f, this.spline.length);

                    location = spline.GetLocationByLength(splineDistance, location.index);
                    system.transform.position = spline.GetPoint(location);
                }

                break;

            case SystemModType.RotationX:
                currentValue = ReachedLimits(currentValue);
                system.transform.rotation = Quaternion.Euler(currentValue, system.transform.rotation.eulerAngles.y, system.transform.rotation.eulerAngles.z);
                break;

            case SystemModType.RotationY:
                currentValue = ReachedLimits(currentValue);
                system.transform.rotation = Quaternion.Euler(system.transform.rotation.eulerAngles.x, currentValue, system.transform.rotation.eulerAngles.z);
                break;

            case SystemModType.RotationZ:
                currentValue = ReachedLimits(currentValue);
                system.transform.rotation = Quaternion.Euler(system.transform.rotation.eulerAngles.x, system.transform.rotation.eulerAngles.y, currentValue);
                break;
        }
    }

    public override void SetNormalizedValue(float newVal, bool useLowPassFilter)
    {
        if (newVal > 0.4 && newVal < 0.6)
        {
            direction = 0;
            okTime += Time.deltaTime;
            if (okTime >= timeToOk)
                GameObject.Find("Manager").GetComponent<AppManager>().ModifierDone();

            return;
        }

        okTime = 0.0f;

        direction = newVal > 0.5 ? 1 : -1;
    }

    public override float GetNormalizedValueForXMLStorage()
    {
        if (modType != SystemModType.Position) 
            return GetCurrentNormalizedValue();

        float totalLength = spline.length;
        float currentLength = spline.GetLength(location);

        return currentLength / totalLength;       
    }

    public void SetSplinePositionByLength(float currentLength)
    {
        if (this.spline)
        {
            if (!spline.circular) this.splineDistance = Mathf.Clamp(this.splineDistance, 0f, this.spline.length);

            location = spline.GetLocationByLength(currentLength);
            system.transform.position = spline.GetPoint(location);
        }
    }


}
