﻿using UnityEngine;
using System.Collections;
using Thinksquirrel.Fluvio.Plugins.Effectors;

public class EffectorData : MonoBehaviour {

    public GameObject effectorObject;
    public FluidEffector effector;

}
