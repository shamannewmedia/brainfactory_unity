﻿using UnityEngine;
using System.Collections.Generic;

public class SystemProperties : MonoBehaviour {

    public GameObject[] theOnesToBeIndicated;

    public Dictionary<string, float> forces = new Dictionary<string, float>();
    public Dictionary<string, float> vorticity = new Dictionary<string, float>();
    public Dictionary<string, float> range = new Dictionary<string, float>();

    public void AddForce(string key, float force)
    {
        forces.Add(key, force);
    }
    public void AddVorticity(string key, float vort)
    {
        vorticity.Add(key, vort);
    }
    public void AddRange(string key, float r)
    {
        range.Add(key, r);
    }

}
