﻿using UnityEngine;
using System.Xml;
using System.Collections.Generic;
using Thinksquirrel.Fluvio;
using Thinksquirrel.Fluvio.Plugins.Effectors;

public class controler : MonoBehaviour {

    public GameObject fluid;
    //public GameObject target;
    public Vector3 target;

    public bool show = true;
    public GameObject emitter;
    public int emittersCount = 3;
    public float particlesLife = 5.0f;

    public int emisionRate = 600;
    
    private float startTime = 0;
    private int particlesEmitted = 0;

    private FluidParticleSystem ps;
    private List<GameObject> emitters = new List<GameObject>();
    private int activeEmitters = 0;
    private int totalParticleEmitters = 0;

    public bool emitParticles = true;

    public void CreateEmitters (XmlNodeList info)
    {
        for (int i = 0; i < info.Count; i++)
        {
            //<Emitter Color="#DF3300"   />
            GameObject e = Instantiate(emitter);

            e.name = info[i].Attributes["Id"].Value;

            if (info[i].Attributes["Speed"] != null)
            {
                float speed = float.Parse(info[i].Attributes["Speed"].Value);
                e.GetComponent<emitter>().speed = speed;
            }

            if (info[i].Attributes["Life"] != null)
            {
                float life = float.Parse(info[i].Attributes["Life"].Value);
                e.GetComponent<emitter>().life = life;
            }

            if (info[i].Attributes["Size"] != null)
            {
                float size = float.Parse(info[i].Attributes["Size"].Value);
                e.GetComponent<emitter>().size = size;
            }

            if (info[i].Attributes["Repeaters"] != null)
            {
                int rep = int.Parse(info[i].Attributes["Repeaters"].Value);
                e.GetComponent<emitter>().repeaters = rep;
            }

            if (info[i].Attributes["Color"] != null)
                e.GetComponent<emitter>().color = Utils.hexToColor(info[i].Attributes["Color"].Value);

            if (info[i].Attributes["Position"] != null)
            {
                string[] position = info[i].Attributes["Position"].Value.Split(',');
                Vector3 pos = new Vector3();
                pos.x = float.Parse(position[0].Trim());
                pos.y = float.Parse(position[1].Trim());
                pos.z = float.Parse(position[2].Trim());
                e.transform.position = pos;
            }

            if (info[i].Attributes["Direction"] != null)
            {
                string[] direction = info[i].Attributes["Direction"].Value.Split(',');
                Vector3 dir = new Vector3();
                dir.x = float.Parse(direction[0].Trim());
                dir.y = float.Parse(direction[1].Trim());
                dir.z = float.Parse(direction[2].Trim());
                e.GetComponent<emitter>().direction = dir;
            }

            if (info[i].Attributes["Active"] != null)
            {
                if (info[i].Attributes["Active"].Value.ToLower().Equals("false"))
                    e.SetActive(false);
                else
                {
                    activeEmitters++;
                    totalParticleEmitters += e.GetComponent<emitter>().repeaters + 1;
                }
            }

            e.transform.parent = GameObject.Find("Emitters").transform;
            e.GetComponent<emitter>().Setup(fluid);
            emitters.Add(e);
        }
    }
    
    // Use this for initialization
    void Start()
    {
        ps = fluid.GetComponent<FluidParticleSystem>();
        ps.GetParticleSystem().Stop();
    }

    // Update is called once per frame
    void FixedUpdate () {
        if (!emitParticles || !show || emitters.Count == 0 || ps.GetParticleSystem().particleCount >= ps.GetParticleSystem().maxParticles)
            return;

        int maxEmisionRate = (int)(ps.GetParticleSystem().maxParticles / particlesLife);

        if (emisionRate > maxEmisionRate)
            emisionRate = maxEmisionRate;

        float timeToEmit = (float)totalParticleEmitters / (float)emisionRate;

        particlesEmitted = 0;

        int emision = Mathf.RoundToInt(Time.deltaTime * emisionRate);
        int timesPP = emision / totalParticleEmitters;
        //Debug.Log(timesPP);
        if (emision == 0 || timesPP == 0)
        {
            startTime += Time.deltaTime;
            if (startTime >= timeToEmit)
                timesPP = Mathf.RoundToInt(timeToEmit * emisionRate) / totalParticleEmitters;
            else
                return;
        }
        startTime = 0;

        Emit(timesPP);
        //Debug.Log(ps.GetParticleSystem().particleCount);
    }

    private void Emit(int times)
    {
        float change = 0.3f;
        //Vector3 direction = target.transform.position;
        Vector3 direction = target;
        direction.x = direction.x + Random.Range(-change, change);
        direction.y = direction.y + Random.Range(-change, change);
        direction.z = direction.z + Random.Range(-change, change);

        for (int i = 0; i < emitters.Count; i++)
        {
            GameObject e = emitters[i];
            if (!e.activeSelf)
                continue;

            for (int t = 0; t < times; t++)
                e.GetComponent<emitter>().EmitParticles(direction);
        }
    }

    #region EXTERNAL FUNCTIONS
   public void EmitParticles(bool emit)
    {
        emitParticles = emit;
    }

    public List<GameObject> GetEmitters ()
    {
        return emitters;
    }

    public FluidParticleSystem GetEmitter (int index)
    {
        if (index < 0 || index >= emitters.Count)
            return null;

        return emitters[index].GetComponent<FluidParticleSystem>();
    }

    public int GetThroughput()
    {
        return emisionRate;
    }

    public void ChangeThroughput (int value)
    {
        emisionRate = value;
    }

    public int GetEmittersNumber()
    {
        int e = 0;
        for (int i = 0; i < emitters.Count; i++)
            if (emitters[i].activeSelf)
                e++;

        return e;
    }

    public int GetEmittersActiveNumber()
    {
        int e = 0;

        if (!emitters[0].activeSelf)
        {
            e = emitters.Count;
            for (int i = 0; i < emitters.Count; i++)
                if (!emitters[i].activeSelf)
                    e++;
        }
        else
        {
            for (int i = 0; i < emitters.Count; i++)
                if (emitters[i].activeSelf)
                    e++;
        }
        
        return e;
    }

    public void ChangeEmittersNumbers (int number)
    {
        if (number < 1) number = 1;

       if (number >= 2 * emitters.Count) number = 2 * emitters.Count - 1;

        if (number == activeEmitters || 2 * emitters.Count - number == activeEmitters)
            return;

        totalParticleEmitters = 0;

        if (number <= emitters.Count)
        {
            for (int i = 0; i < emitters.Count; i++)
            {
                if (i < number)
                {
                    emitters[i].SetActive(true);
                    totalParticleEmitters += emitters[i].GetComponent<emitter>().repeaters + 1;
                }
                else
                    emitters[i].SetActive(false);
            }

            activeEmitters = number;
        }
        else
        {
            for (int i = 0; i < emitters.Count; i++)
            {
                if (i >= number - emitters.Count)
                {
                    emitters[i].SetActive(true);
                    totalParticleEmitters += emitters[i].GetComponent<emitter>().repeaters + 1;
                }
                else
                    emitters[i].SetActive(false);
            }
            activeEmitters =  2 * emitters.Count - number;
        }

    }

    public void ChangeEmittersColor (Color32 color)
    {
        foreach(GameObject emitter in emitters)
            emitter.GetComponent<emitter>().color = color;
    }
    #endregion
}
