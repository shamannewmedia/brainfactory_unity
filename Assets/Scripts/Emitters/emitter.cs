﻿using UnityEngine;
using System.Collections;

public class emitter : MonoBehaviour {

    public Vector3 direction = Vector3.one;
    public float speed = 1;
    public float life = 5;
    public float size = 2;
    public Color32 color = new Color32(255, 0, 0, 0);

    public int repeaters = 3;
    public float randomness = 0.5f;

    private GameObject fluid;

    private ParticleSystem ps;

    private bool fixedDirection = false;

    void Awake ()
    {
        transform.LookAt(direction);
    }
    
    public void Setup (GameObject fluid)
    {
        this.fluid = fluid;
        ps = fluid.GetComponent<ParticleSystem>();

        fixedDirection = false;
    }

    public void EmitParticles(Vector3 direction)
    {
        if (fixedDirection)
            this.direction = direction;

        ParticleSystem.EmitParams theEmitter = new ParticleSystem.EmitParams();
        theEmitter.position = transform.position;
        
        theEmitter.startLifetime = life;
        theEmitter.startColor = color;
        theEmitter.startSize = size;
        theEmitter.velocity = GetVelocity();

        ps.Emit(theEmitter, 1);
        foreach (ParticleSystem.EmitParams e in GetEmitters(theEmitter))
        {
            ps.Emit(e, 1);
        }
    }

    private Vector3 GetVelocity()
    {        
        float x = direction.x - transform.position.x;
        float y = direction.y - transform.position.y;
        float z = direction.z - transform.position.z;

        Vector3 v = new Vector3(x, y, z);
        v = new Quaternion(Random.Range(0, randomness), Random.Range(0, randomness), Random.Range(0, randomness), 0) * v;

        float length = Mathf.Sqrt(x * x + y * y + z * z);
        float scale = speed / length;

        v *= scale;

        return v;
    }

    private ParticleSystem.EmitParams[] GetEmitters(ParticleSystem.EmitParams theEmitter)
    {
        ParticleSystem.EmitParams[] emitters = new ParticleSystem.EmitParams[repeaters];
        float dist = 0.5f;
        for (int i = 1; i <= repeaters; i++)
        {
            ParticleSystem.EmitParams e = new ParticleSystem.EmitParams();
            e.position = new Vector3(theEmitter.position.x + Random.Range(-dist, dist), theEmitter.position.y + Random.Range(-dist, dist), theEmitter.position.z + Random.Range(-dist, dist));
            e.velocity = GetVelocity();
            e.startColor = theEmitter.startColor;
            e.startLifetime = theEmitter.startLifetime;
            e.startSize = theEmitter.startSize;

            emitters[i-1] = e;
        }
        return emitters;
    }

    #region EXTERNAL FUNCTIONS
    public void setPosition (Vector3 position) { transform.position = position; }
    public void setStartSpeed (float speed) { this.speed = speed; }
    public void setDirection (Vector3 direction) { this.direction = direction; }
    public void setSize (float size) { this.size = size; }
    public void setLifeTime (float life) { this.life = life; }
    #endregion
}
