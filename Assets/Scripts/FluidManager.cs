﻿using UnityEngine;
using System;
using System.IO;
using System.Xml;
using Thinksquirrel.Fluvio;
using Thinksquirrel.Fluvio.Plugins.Effectors;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

public class FluidManager : MonoBehaviour {

	public FluidParticleSystem fluidParticleSystem;
    public GameObject fluidControler;

    public string IP = "127.0.0.1";
	public int Port = 5000;

    private string delimeter = "[/UDP]";
    private ParticleSystem.Particle[] particles;
	private int particleCount;

    public int FrameCaptureQuantity;
    public int FramesToSkipBetweenCapture;

    private int CurrentFrameCaptureAmount;
    private int CurrentSkippedFramesInCapture;

    private bool IsProcessing;
    private bool SkippingFrames;
    private string lastProcessedFile;

    //private CommManager connection;

    private UdpClient udpConnection;

	private string basePath = "";

	private List<FluidEllipsoidEffector> fEEs;
	private List<FluidPointEffector> fPEs;

    // End Indicators section

    public AppManager manager;

    public Indicators theIndicator;

	// Use this for initialization
	void Start () {
        
        //FrameCaptureQuantity = 1;
        //FramesToSkipBetweenCapture = 0;

        IsProcessing = false;
        SkippingFrames = false;
        lastProcessedFile = "";

        CurrentFrameCaptureAmount = FrameCaptureQuantity;
        CurrentSkippedFramesInCapture = FramesToSkipBetweenCapture;

        udpConnection = new UdpClient();
        udpConnection.Connect(IP, Port);

        //connection = new CommManager ();

        fluidParticleSystem.GetParticleSystem ().Stop ();
    }

    public void EffectorsLists()
    {
        IEnumerable<FluidEllipsoidEffector> fEEsEnum = this.fluidParticleSystem.GetPlugins<FluidEllipsoidEffector>();

        fEEs = new List<FluidEllipsoidEffector>();
        //EEIndicators = new List<GameObject>();
        if (fEEsEnum != null)
        {
            foreach (FluidEllipsoidEffector item in fEEsEnum)
                fEEs.Add(item);
        }
        IEnumerable<FluidPointEffector> fPEsEnum = this.fluidParticleSystem.GetPlugins<FluidPointEffector>();

        fPEs = new List<FluidPointEffector>();
        if (fPEsEnum != null)
        {
            foreach (FluidPointEffector item in fPEsEnum)
                fPEs.Add(item);
        }
            
    }

    public bool CreateEffectors(XmlNodeList effectors, string type)
    {
        if (effectors.Count == 0)
            return true;

        if (!type.Equals("point") && !type.Equals("ellipsoid") && !type.Equals("cube"))
            return false;

        GameObject theEffectors = new GameObject();
        theEffectors.name = type + "Effectors";
        theEffectors.transform.parent = fluidControler.transform;

        for (int i = 0; i < effectors.Count; i++)
        {
            GameObject newEffector = CreateEffector(effectors[i], type);
            if (newEffector == null)
                return false;

            newEffector.transform.SetParent(theEffectors.transform);

            theIndicator.CreateEffectorIndicator(newEffector);
        }

        return true;
    }

    private GameObject CreateEffector(XmlNode effector, string type) {
        GameObject newEffector = new GameObject();
        FluidEffector eff;
        switch (type)
        {
            case "point":
                newEffector.AddComponent<FluidPointEffector>();
                eff = newEffector.GetComponent<FluidPointEffector>();
                break;

            case "ellipsoid":
                newEffector.AddComponent<FluidEllipsoidEffector>();
                eff = newEffector.GetComponent<FluidEllipsoidEffector>();
                break;

            case "cube":
                newEffector.AddComponent<FluidCubeEffector>();
                eff = newEffector.GetComponent<FluidCubeEffector>();
                break;
            default:
                return null;
        }

        
        newEffector.name = effector.Attributes["Id"].Value;

        if (effector.Attributes["ForceType"] != null)
        {
            string forceType = effector.Attributes["ForceType"].Value.ToLower();
            eff.forceType = forceType.Equals("directional") ? FluidEffectorForceType.Directional : FluidEffectorForceType.Radial;
        }

        if (effector.Attributes["ForceAxis"] != null)
        {
            string forceAxis = effector.Attributes["ForceAxis"].Value.ToLower();
            switch (forceAxis)
            {
                case "x":
                    eff.forceAxis = FluidEffectorForceAxis.X;
                    break;

                case "y":
                    eff.forceAxis = FluidEffectorForceAxis.Y;
                    break;

                default:
                    eff.forceAxis = FluidEffectorForceAxis.Z;
                    break;

            }
        }

        if (effector.Attributes["Force"] != null)
        {
            float force = float.Parse(effector.Attributes["Force"].Value);
            FluvioMinMaxCurve f = new FluvioMinMaxCurve();
            f.maxConstant = force;
            f.minConstant = force;
            eff.force = f;

        }

        if (effector.Attributes["Vorticity"] != null)
        {
            float vorticity = float.Parse(effector.Attributes["Vorticity"].Value);
            FluvioMinMaxCurve v = new FluvioMinMaxCurve();
            v.maxConstant = vorticity;
            v.minConstant = vorticity;
            eff.vorticity = v;
        }

        if (effector.Attributes["Range"] != null)
        {
            float range = float.Parse(effector.Attributes["Range"].Value);
            eff.effectorRange = range;
        }

        if (effector.Attributes["DecayType"] != null)
        {
            string decayType = effector.Attributes["DecayType"].Value.ToLower();
            switch (decayType)
            {
                case "keep alive":
                    eff.decayType = FluidEffectorDecayType.KeepAlive;
                    break;

                case "decay":
                    eff.decayType = FluidEffectorDecayType.Decay;
                    break;

                default:
                    eff.decayType = FluidEffectorDecayType.None;
                    break;

            }
        }

        if (effector.Attributes["Decay"] != null)
        {
            float decay = float.Parse(effector.Attributes["Decay"].Value);
            eff.decay = decay;
        }

        if (effector.Attributes["DecayJitter"] != null)
        {
            float decayJitter = float.Parse(effector.Attributes["DecayJitter"].Value);
            eff.decayJitter = decayJitter;
        }

        if (effector.Attributes["Position"] != null)
        {
            string[] position = effector.Attributes["Position"].Value.Split(',');
            Vector3 pos = new Vector3();
            pos.x = float.Parse(position[0].Trim());
            pos.y = float.Parse(position[1].Trim());
            pos.z = float.Parse(position[2].Trim());
            eff.position = pos;
        }

        if (effector.Attributes["Active"] != null && effector.Attributes["Active"].Value.ToLower().Equals("false"))
            newEffector.SetActive(false);

        return newEffector;
    }

    public bool CreateSystems(XmlNodeList system)
    {
        GameObject systems = new GameObject();
        systems.name = "Systems";
        systems.transform.parent = fluidControler.transform;

        for (int i = 0; i < system.Count; i++)
        {
            GameObject sys = new GameObject();
            sys.name = system[i].Attributes["Id"].Value;
            sys.AddComponent<SystemProperties>();
             
            XmlNodeList effectors = system[i].ChildNodes;
            for (int e = 0; e < effectors.Count; e++)
            {
                string type = effectors[e].Name.ToLower();
                GameObject eff = CreateEffector(effectors[e], type);
                eff.name = sys.name + "_" + eff.name;
                eff.transform.parent = sys.transform;
               
                sys.GetComponent<SystemProperties>().AddForce(eff.name, eff.GetComponent<FluidEffector>().force.maxConstant);
                sys.GetComponent<SystemProperties>().AddRange(eff.name, eff.GetComponent<FluidEffector>().effectorRange);
                sys.GetComponent<SystemProperties>().AddVorticity(eff.name, eff.GetComponent<FluidEffector>().vorticity.maxConstant);
            }

            if (system[i].Attributes["Position"] != null)
            {
                string[] position = system[i].Attributes["Position"].Value.Split(',');
                Vector3 pos = new Vector3();
                pos.x = float.Parse(position[0].Trim());
                pos.y = float.Parse(position[1].Trim());
                pos.z = float.Parse(position[2].Trim());
                sys.transform.position = pos;
            }

            if (system[i].Attributes["Rotation"] != null)
            {
                string[] rotation = system[i].Attributes["Rotation"].Value.Split(',');
                Vector3 rot = new Vector3();
                rot.x = float.Parse(rotation[0].Trim());
                rot.y = float.Parse(rotation[1].Trim());
                rot.z = float.Parse(rotation[2].Trim());
                sys.transform.Rotate(rot);
            }
            sys.transform.parent = systems.transform;

            if (system[i].Attributes["Active"] != null && system[i].Attributes["Active"].Value.ToLower().Equals("false"))
                sys.SetActive(false);

            theIndicator.CreateSystemIndicators(sys);
         }

        return true;
    }

    public void CreateEmitters(XmlNodeList emitters)
    {
        fluidControler.GetComponent<controler>().CreateEmitters(emitters);
    }

	public void ConnectToServer(){
		//if (!connection.IsConnected ()) {
		//	Debug.Log (connection.fnConnectResult (IP, Port));
        //    UsingConnection = true;
        //}
	}

	public void ProcessParticles(){

        bool doExportFile = false;
      
        if (this.IsProcessing == false)
        {

            this.IsProcessing = true;
            this.SkippingFrames = false;
            this.CurrentFrameCaptureAmount = 1;
            this.CurrentSkippedFramesInCapture = 0;

            // we begin by capturing the first frame
            this.GetParticlesReference(this.CurrentFrameCaptureAmount);

            Debug.Log("Frame captured:" + this.CurrentFrameCaptureAmount.ToString());
            
            if (CurrentFrameCaptureAmount > this.FrameCaptureQuantity)
            {
                //we have ended capture
                doExportFile = true;
            }
            else
            {   
                //we keep capturing frames
                this.SkippingFrames = true;
            }

        }

        if (doExportFile)
        {
            ExportFileAndSendData();
        }

	}

    private void UpdateProcessParticles()
    { 
        if (SkippingFrames){
            this.CurrentSkippedFramesInCapture += 1;
            if (this.CurrentSkippedFramesInCapture > this.FramesToSkipBetweenCapture)
            {
                this.CurrentSkippedFramesInCapture = 0;
                this.SkippingFrames = false;
            }
            else
            {
                Debug.Log("Frame Skipped:" + CurrentSkippedFramesInCapture.ToString());

            }
        }
        else
        {
            this.CurrentFrameCaptureAmount += 1;
            if (this.CurrentFrameCaptureAmount <= this.FrameCaptureQuantity)
            {
                this.GetParticlesReference(this.CurrentFrameCaptureAmount);
                this.SkippingFrames = true;
                Debug.Log("Frame captured:" + this.CurrentFrameCaptureAmount.ToString());
            }
            else
            {
                //we have finished
                this.ExportFileAndSendData();
            }
        } 
    }


   
    private void ExportFileAndSendData()
    {
        string fileName = "";
        string directoryPath = this.basePath;
        fileName = "ParticleExport_" + DateTime.Now.Ticks.ToString() + ".txt";
        this.SaveToFile(fileName);

        //this.SaveMetaData();
        CommDataType dt = new CommDataType();
        dt.keepListening = true;
        dt.inputDirectoryPath = directoryPath;
        dt.inputFileName = fileName;
        //this.connection.SendData (JsonUtility.ToJson (dt));

        //sending data to RealFlow
        Byte[] sendBytes = Encoding.ASCII.GetBytes(JsonUtility.ToJson(dt) + delimeter);
        udpConnection.Send(sendBytes, sendBytes.Length);

        this.lastProcessedFile = fileName;

        Debug.Log("Processing ended");

        //frame harvest has ended
        this.CurrentFrameCaptureAmount = 0;
        this.CurrentSkippedFramesInCapture = 0;
        this.IsProcessing = false;
        this.SkippingFrames = false;
    }

	public void SendStopProcessing (){
		if (udpConnection != null) {
			CommDataType dt = new CommDataType ();
			dt.keepListening = false;
			dt.inputDirectoryPath = "";
			dt.inputFileName = "";
            Byte[] sendBytes = Encoding.ASCII.GetBytes(JsonUtility.ToJson(dt) + delimeter);
            udpConnection.Send(sendBytes, sendBytes.Length);
        } else {
			Debug.Log("Not able to send message. Server Not available.");
		}
	}

	// Update is called once per frame
	void Update () {
        if (IsProcessing)
        {
            UpdateProcessParticles();
        }
    }
    

	void GetParticlesReference(int currentCaptureFrame){

		InitializeIfNeeded ();

        int indexFrameOffset = (currentCaptureFrame - 1) * (particles.Length / this.FrameCaptureQuantity);

        ParticleSystem.Particle[] mainParticles;

		if (fluidParticleSystem != null) {

			mainParticles = new ParticleSystem.Particle[fluidParticleSystem.GetParticleSystem ().maxParticles];

			particleCount = fluidParticleSystem.GetParticleSystem().GetParticles (mainParticles);
            
			//copying mainParticles to this.particles array

			int particleIndex = mainParticles.Length;

			Array.Copy (mainParticles, 0, particles, indexFrameOffset, mainParticles.Length);
           
            //transforming mainParticles positions
			for (int pindex = 0; pindex < mainParticles.Length; pindex++) {
                //particles[pindex].position = RotatePointAroundPivot(particles[pindex].position, Vector3.zero, mainFluidParticleSystem.transform.rotation.eulerAngles);
                particles [pindex + indexFrameOffset].position = particles [pindex + indexFrameOffset].position + fluidParticleSystem.transform.position;
            }

		}
	}

	void SaveToFile(string filePath){
		//string basePath = Application.dataPath;

		using (StreamWriter sw = new StreamWriter(basePath + "\\" + filePath)) 
		{
			// Add some text to the file.
			foreach (ParticleSystem.Particle p in particles) {
				sw.WriteLine(p.position.x.ToString() + " " + p.position.y.ToString() + " " + p.position.z.ToString());
			}
			sw.Close();
		}
	
	}

	void InitializeIfNeeded(){
		if (fluidParticleSystem != null) {

			//getting max number of particles for this particle snapshot.

			int maxParticles = fluidParticleSystem.GetParticleSystem ().maxParticles;

            //we multiply by the amount of frames to be captured
            maxParticles = maxParticles * this.FrameCaptureQuantity;

            if (particles == null || particles.Length < maxParticles)
				particles = new ParticleSystem.Particle[maxParticles]; 
		}
	}

	public void PlayMainFluid(){
		this.fluidParticleSystem.GetParticleSystem ().Play ();
        fluidControler.GetComponent<controler>().EmitParticles(true);
    }

	public void StopMainFluid(){
		this.fluidParticleSystem.GetParticleSystem ().Stop ();
        fluidControler.GetComponent<controler>().EmitParticles(false);
	}

    public FluidEffector GetEffectorByIdentifier (string effectorId){

		string[] effectorParams = effectorId.Split('_');

		FluidEffector result = null;

		if (effectorParams.Length > 0) {

			switch (effectorParams[0])
			{
			case "E":
				//Ellipsoid effector

				if (effectorParams.Length == 2) {
					int effectorIndex = int.Parse (effectorParams [1]);

					if (effectorIndex <= fEEs.Count - 1) {
						result = fEEs [effectorIndex];
					}

				}
					
				break;
			case "P":
				//Point effector

				if (effectorParams.Length == 2) {
					int effectorIndex = int.Parse (effectorParams [1]);

					if (effectorIndex <= fPEs.Count - 1) {
						result = fPEs [effectorIndex];
					}

				}

				break;
		
			default:
				Debug.Log("Invalid effector id");
				break;
			}
		}

		return result;

	}
    
    public FluidParticleSystem GetFluid()
    {
        return fluidParticleSystem;
    }


    public FluidParticleSystem GetFluidByIdentifier(int fluidId) {
        return fluidControler.GetComponent<controler>().GetEmitter(fluidId);
    }

	public bool IsMainFluid(string fluidId){
		bool result = false;

		string[] fluidParams = fluidId.Split ('_');

		if (fluidParams.Length > 0) {

			switch (fluidParams[0])
			{
			case "FM":
				//Main Fluid.

				result = true;

				break;
			case "FS":
				//Secondary fluid.

				result = false;

				break;

			default:
				Debug.Log("Invalid fluid id");
				break;
			}
		}

		return result;
	}

    public void SetBasePath(string bP)
    {
        this.basePath = bP;
    }

    public List<GameObject> GetFluidEmitterList()
    {
        return fluidControler.GetComponent<controler>().GetEmitters();
    }

    public Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles)
    {
        Vector3 dir = point - pivot; // get point direction relative to pivot
        dir = Quaternion.Euler(angles) * dir; // rotate it
        point = dir + pivot; // calculate rotated point
        return point; // return it
    }

    public string GetLastProcessedFileName()
    {
        return this.lastProcessedFile;
    }

    public bool GetIsProcessing()
    {
        return IsProcessing;
    }

    public int GetThroughput()
    {
        return fluidControler.GetComponent<controler>().GetThroughput();
    }

    public void ChangeThroughput(int value)
    {
        fluidControler.GetComponent<controler>().ChangeThroughput(value);
    }

    public int GetEmittersNumber ()
    {
        return fluidControler.GetComponent<controler>().GetEmittersActiveNumber();
    }

    public void ChangeEmittersNumbers (int value)
    {
        fluidControler.GetComponent<controler>().ChangeEmittersNumbers(value);
    }

    public void ChangeEmittersColor(Color32 color)
    {
        fluidControler.GetComponent<controler>().ChangeEmittersColor(color);
    }

    public int GetParticleCount()
    {
        return fluidParticleSystem.GetParticleSystem().particleCount;
    }
}
