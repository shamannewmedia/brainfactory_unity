﻿using UnityEngine;
using System.Collections;

public class CalibrationView : MonoBehaviour {

    public ShapeOfMind shapeOfMind;
    public Camera calibrationCamera;

    public CalibrationTextManager textManager;

    public void StartCalibrating()
    {
        shapeOfMind.StartCalibration();
    }

    public void CalibrationFinished()
    {
        GameObject.Find("Manager").GetComponent<AppManager>().CalibrationFinished();
    }
    
    public void Show()
    {
        gameObject.SetActive(true);
        calibrationCamera.enabled = true;
    }

    public void Hide()
    {
        shapeOfMind.calibrate = false;
        gameObject.SetActive(false);

        textManager.HideAll();
        calibrationCamera.enabled = false;
    }

}
