﻿using UnityEngine;
using System.Collections;

public class FluidView : MonoBehaviour {

    public Camera fluidCamera;

    public void Hide()
    {
        //gameObject.SetActive(false);
        fluidCamera.enabled = false;
    }

    public void Show()
    {
        //gameObject.SetActive(true);
        fluidCamera.enabled = true;
    }

}
