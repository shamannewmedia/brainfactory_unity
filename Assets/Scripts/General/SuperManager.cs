﻿using UnityEngine;
using System.Collections;

public class SuperManager : MonoBehaviour {

    public enum State { Idle, Calibrating, Shaping, Processing };
    private State state = State.Idle;

    public GameObject fluid;
    public GameObject calibration;
    public GameObject brainWave;
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void CalibrationFinished()
    {
        state = State.Shaping;
        //fluid.SetActive(true);
        //calibration.SetActive(false);
    }
}
