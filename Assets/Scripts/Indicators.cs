﻿using UnityEngine;
using System.Collections.Generic;
using Thinksquirrel.Fluvio.Plugins.Effectors;

public class Indicators : MonoBehaviour {

    public GameObject indicatorPrefab;

    public GameObject UI;
    
    private Dictionary<string, GameObject> indicators = new Dictionary<string, GameObject>();
    private string lastEffector = "nothing to show yet";

    public bool showIndicators = false;

	// Use this for initialization
	void Start () {
        UI.SetActive(showIndicators);
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp("v"))
        {
            showIndicators = !showIndicators;
            UI.SetActive(showIndicators);

            if (!lastEffector.Equals("nothing to show yet"))
                indicators[lastEffector].SetActive(showIndicators);
        }
        if (!lastEffector.Equals("nothing to show yet"))
            if (showIndicators)
            {
                GameObject i = indicators[lastEffector];
                if (i.GetComponent<EffectorData>() != null)
                {
                    i.transform.position = i.GetComponent<EffectorData>().effector.worldPosition;
                    
                }
                else
                {
                    foreach (Transform child in i.transform)
                    {
                        child.position = child.GetComponent<EffectorData>().effector.worldPosition;
                    }

                }
            }
            
    }
    public void ChangeEffector(string effector)
    {
        if (effector == "")
        {
            lastEffector = "nothing to show yet";
            return;
        }

        if (!lastEffector.Equals("nothing to show yet"))
            indicators[lastEffector].SetActive(false);

        lastEffector = "Indicator_" + effector;
        indicators[lastEffector].SetActive(showIndicators);
    }

    public void NoEffectorToShow()
    {
        lastEffector = "nothing to show yet";
    }

    private GameObject CreateIndicator(GameObject stuff)
    {
        GameObject i = (GameObject)Instantiate(indicatorPrefab, stuff.GetComponent<FluidEffector>().position, Quaternion.identity);
        i.GetComponent<MeshRenderer>().material.color = stuff.GetComponent<FluidEffector>().force.maxConstant > 0 ? Color.blue : Color.red;
        i.AddComponent<EffectorData>();
        i.GetComponent<EffectorData>().effector = stuff.GetComponent<FluidEffector>();
        i.transform.parent = GameObject.Find("Verbose").transform;
        return i;
    }

    public void CreateSystemIndicators(GameObject system)
    {
        GameObject sysInd = new GameObject();
        sysInd.name = "Indicator_" + system.name;
        foreach (Transform effector in system.transform)
        {
            GameObject i = CreateIndicator(effector.gameObject);
            i.transform.parent = sysInd.transform;
        }
        sysInd.transform.parent = GameObject.Find("Verbose").transform;
        indicators.Add(sysInd.name, sysInd);
        sysInd.SetActive(false);
    }

    public void CreateEffectorIndicator(GameObject effector)
    {
        GameObject effInd = CreateIndicator(effector);
        effInd.name = "Indicator_" + effector.name;
        indicators.Add(effInd.name, effInd);
        effInd.SetActive(false);
    }
}
