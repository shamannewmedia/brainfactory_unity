﻿using UnityEngine;
using System;
using System.Collections;
using Rug.Osc;

public class CalibratingOSCSender : MonoBehaviour
{

    public OSCSender Sender;
    public float FPS_To_Send = 10;
    public bool EnableOSC;
    public float lastSentIndexOSCTimestamp;
    private float blendWeight = 0;
    private SkinnedMeshRenderer skinnedMeshRenderer;

    private AppManager manager;

    public ShapeOfMind shapeOfMind;

    // Use this for initialization
    void Start()
    {
        manager = GameObject.Find("Manager").GetComponent<AppManager>();
        lastSentIndexOSCTimestamp = Time.realtimeSinceStartup;
        skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (manager.GetState() != 1)
            return;

        if (EnableOSC)
        {
            float intervalTime = 1.0f / (float)FPS_To_Send;
            if ((Time.realtimeSinceStartup - lastSentIndexOSCTimestamp) > (intervalTime))
            {
                lastSentIndexOSCTimestamp = Time.realtimeSinceStartup;
                SendBeatValue();
            }
        }

    }

    private void SendBeatValue()
    {
        object[] parameters = new object[1];
        //Debug.Log(blendWeight);
        int state = shapeOfMind.GetState();
        string address = "/calibrating" + state;
        float val = state == 1 ? shapeOfMind.GetDistState1() : shapeOfMind.GetWave();

        parameters[0] = val;
        OscMessage m = new OscMessage(address, parameters);
        Sender.storeElementInQueue(m);

        if (state == 1)
        {
            float distanceVal = shapeOfMind.GetDistState1();
            string addressD1 = "/distance1";
            object[] parametersD1 = new object[1];
            parametersD1[0] = distanceVal;
            OscMessage mD1 = new OscMessage(addressD1, parametersD1);
            Sender.storeElementInQueue(mD1);
        }

        if (state == 2)
        {
            float distanceVal = shapeOfMind.GetDistState2();
            string addressD2 = "/distance2";
            object[] parametersD2 = new object[1];
            parametersD2[0] = distanceVal;
            OscMessage mD2 = new OscMessage(addressD2, parametersD2);
            Sender.storeElementInQueue(mD2);
        }

        if(state == 3)
        {
            float distanceVal = shapeOfMind.GetDistState3();
            string addressD3 = "/distance3";
            object[] parametersD3 = new object[1];
            parametersD3[0] = distanceVal;
            OscMessage mD3 = new OscMessage(addressD3, parametersD3);
            Sender.storeElementInQueue(mD3);
        }

    }

    public void SetEnableOSC(bool enableOSC)
    {
        this.EnableOSC = enableOSC;
    }
}