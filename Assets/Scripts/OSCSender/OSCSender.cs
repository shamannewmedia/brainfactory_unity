﻿using UnityEngine;
using System;
using System.Collections.Generic;
//using Ventuz.OSC;
using Rug.Osc;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml;


public class OSCSender : MonoBehaviour
{
    //private OscManager handler = null;

    private OscSender sender = null;

    public int destinationPort;
    private IPAddress address = IPAddress.Parse("127.0.0.1");

    private List<OscMessage> receivedMessages;
    public string clientIp;

    bool active = false;

    private bool recording = false;
    private List<Tuple> records;
    private string basePath = "";

    public GameObject recordingText;
    private string fileName;

    public bool canRecord = false;

    public void SetParameters(int port, string address, bool record)
    {
        destinationPort = port;
        this.address = IPAddress.Parse(address);

        canRecord = record;
        recordingText.GetComponent<Blinking>().isRecording = record;

    }

    public void StartSending()
    {
        if (canRecord)
            records = new List<Tuple>();

        receivedMessages = new List<OscMessage>();
        if (sender == null)
        {
            sender = new OscSender(address, destinationPort);
            sender.Connect();
        }
        active = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!active)
            return;

        try
        {
            sendOSC();
        }
        catch (Exception err)
        {
            print(err.ToString());
        }
    }

    // receive thread
    private void SendData()
    {
        try
        {
            sendOSC();
        }
        catch (Exception err)
        {
            print(err.ToString());
        }

    }

    private void sendOSC()
    {
        foreach (OscMessage message in receivedMessages)
        {
            sender.Send(message);
            if (canRecord && recording)
                records.Add(new Tuple(message));
        }
        receivedMessages.Clear();
    }

    public void storeElementInQueue(OscMessage m)
    {
        if (receivedMessages != null)
            receivedMessages.Add(m);
    }

    public void SetBasePath(string bP)
    {
        this.basePath = bP;
    }

    public void ClearRecords()
    {
        if (!canRecord)
            return;

        if (records != null)
            records.Clear();
        else
            records = new List<Tuple>();
    }

    public void StartRecording() {
        if (!canRecord)
            return;

        recording = true;
        if (records == null)
            records = new List<Tuple>();
        else
            records.Clear(); // SHA_ToDo: review this. could be changed to new.
    }
    public void StopRecording() { recording = false; }
    public void StoreRecords()
    {
        if (!canRecord || records == null || records.Count == 0)
            return;

        fileName = "";
        fileName = "SenderExport_" + DateTime.Now.Ticks.ToString() + ".xml";

        XmlDocument doc = GetDocument();

        doc.Save(basePath + "\\Recordings\\" + fileName);
        records.Clear();

        Debug.Log("File saved!");
    }

    private XmlDocument GetDocument()
    {
        XmlDocument doc = new XmlDocument();
        XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
        XmlElement root = doc.DocumentElement;
        doc.InsertBefore(xmlDeclaration, root);

        XmlElement messages = doc.CreateElement("Messages");
        doc.AppendChild(messages);

        foreach (Tuple t in records)
        {
            XmlElement element = doc.CreateElement("OSCMessage");

            XmlAttribute address = doc.CreateAttribute("Address");
            address.Value = t.GetAddress();
            element.Attributes.Append(address);

            XmlAttribute timestamp = doc.CreateAttribute("Timestamp");
            timestamp.Value = t.GetTimestamp();
            element.Attributes.Append(timestamp);

            foreach(object o in t.GetValues())
            {
                XmlElement val = doc.CreateElement("Parameter");

                XmlAttribute type = doc.CreateAttribute("Type");
                type.Value = GetTypeName(o.GetType().ToString());
                val.Attributes.Append(type);

                XmlAttribute value = doc.CreateAttribute("Value");
                value.Value = o.ToString();
                val.Attributes.Append(value);

                element.AppendChild(val);
            }

            messages.AppendChild(element);
        }

        return doc;
    }

    public string GetFileName()
    {
        return fileName;
    }

    private string GetTypeName(string type)
    {
        if (type == "System.Single")
            return "float";

        if (type == "System.Int32")
            return "int";

        return "";
    }
}


public class Tuple
{
    private OscMessage message;
    private string timeStamp;

    public Tuple(OscMessage message)
    {
        this.message = message;
        this.timeStamp = DateTime.Now.Ticks.ToString();
    }

    public string GetTimestamp()
    {
        return timeStamp;
    }

    public string GetAddress()
    {
        return message.Address;
    }

    public List<object> GetValues()
    {
        List<object> ret = new List<object>();
        foreach (object o in message)
            ret.Add(o);

        return ret;
    }
}