﻿using UnityEngine;
using System;
using System.Collections;
using Rug.Osc;

public class ShapingOSCSender : MonoBehaviour
{

    public OSCSender Sender;
    public float FPS_To_Send = 10;
    public bool EnableOSC;
    public float lastSentIndexOSCTimestamp;
    private float blendWeight = 0;
    private SkinnedMeshRenderer skinnedMeshRenderer;

    private AppManager manager;

    public ShapeOfMind shapeOfMind;

    // Use this for initialization
    void Start()
    {
        manager = GameObject.Find("Manager").GetComponent<AppManager>();
        lastSentIndexOSCTimestamp = Time.realtimeSinceStartup;
        skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (manager.GetState() != 3)
            return;

        if (EnableOSC)
        {
            float intervalTime = 1.0f / (float)FPS_To_Send;
            if ((Time.realtimeSinceStartup - lastSentIndexOSCTimestamp) > (intervalTime))
            {
                lastSentIndexOSCTimestamp = Time.realtimeSinceStartup;
                SendBeatValue();
            }
        }

    }

    private void SendBeatValue()
    {
        object[] parameters = new object[1];
        //Debug.Log(blendWeight);
        float value = shapeOfMind.GetWave();
        string address = "/brainwave";
        parameters[0] = value;
        OscMessage m = new OscMessage(address, parameters);
        Sender.storeElementInQueue(m);

        value = shapeOfMind.GetDecision();
        address = "/decision";
        object[] par = new object[1];
        par[0] = value;
        m = new OscMessage(address, par);
        Sender.storeElementInQueue(m);

        address = "/concept";
        object[] conceptName = new object[1];
        conceptName[0] = manager.GetCurrentConceptName();
        m = new OscMessage(address, conceptName);
        Sender.storeElementInQueue(m);

        address = "/dnaindex";
        object[] dnaindex = new object[1];
        dnaindex[0] = manager.GetCurrentDNAIndex();
        m = new OscMessage(address, dnaindex);
        Sender.storeElementInQueue(m);
    }

    public void SetEnableOSC(bool enableOSC)
    {
        this.EnableOSC = enableOSC;
    }
}