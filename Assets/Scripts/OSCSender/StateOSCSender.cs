﻿using UnityEngine;
using System;
using System.Collections;
using Rug.Osc;

public class StateOSCSender : MonoBehaviour
{

    public OSCSender Sender;
    public float FPS_To_Send = 10;
    public bool EnableOSC;
    public float lastSentIndexOSCTimestamp;
    private float blendWeight = 0;
    private SkinnedMeshRenderer skinnedMeshRenderer;

    // Use this for initialization
    void Start()
    {
        lastSentIndexOSCTimestamp = Time.realtimeSinceStartup;
        skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (EnableOSC)
        {
            float intervalTime = 1.0f / (float)FPS_To_Send;
            if ((Time.realtimeSinceStartup - lastSentIndexOSCTimestamp) > (intervalTime))
            {
                lastSentIndexOSCTimestamp = Time.realtimeSinceStartup;
                SendBeatValue();
            }
        }

    }

    private void SendBeatValue()
    {
        object[] parameters = new object[1];
        //Debug.Log(blendWeight);
        int state = GameObject.Find("Manager").GetComponent<AppManager>().GetState();
        parameters[0] = state;
        OscMessage m = new OscMessage("/state", state);
        Sender.storeElementInQueue(m);
    }

    public void SetEnableOSC(bool enableOSC)
    {
        this.EnableOSC = enableOSC;
    }
}