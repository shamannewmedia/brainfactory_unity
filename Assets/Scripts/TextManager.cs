﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextManager : MonoBehaviour {

    public GameObject panel;

    public GameObject conceptPanel;
    public GameObject concept;
    public GameObject conceptAmount;
    public GameObject koreanConcept;

    public GameObject text;
    public GameObject englishText;
    public GameObject koreanText;

    public GameObject shaped;

    public float speed = 0.001f;
    public float conceptTime = 4;
    public float textTime = 5;
    public float shapingTime = 3;

    private Coroutine fadeAll = null;
    private Coroutine fadeShaping = null;
    private Coroutine hideShaping = null;

    void Start()
    {
        panel.GetComponent<CanvasGroup>().alpha = 0;
        conceptPanel.GetComponent<CanvasGroup>().alpha = 0;
        text.GetComponent<CanvasGroup>().alpha = 0;
        shaped.GetComponent<CanvasGroup>().alpha = 0;
    }

    public void SetValues(string englishText, string koreanText, float conceptTime, float textTime, float shapingTime)
    {
        this.englishText.GetComponent<Text>().text = englishText.ToLower();
        this.koreanText.GetComponent<Text>().text = koreanText;
        this.conceptTime = conceptTime;
        this.textTime = textTime;
        this.shapingTime = shapingTime;
    }

    public void ShowText(string concept, int conceptAmount, string koreanConcept)
    {
        ShowThisThings(concept, conceptAmount, koreanConcept);
    }

    public void ShowText(string concept, int conceptAmount, string koreanConcept, string english, string korean)
    {
        SetValues(english, korean, conceptTime, textTime, shapingTime);
        ShowThisThings(concept, conceptAmount, koreanConcept);
    }

    private void ShowThisThings(string concept, int conceptAmount, string koreanConcept)
    {
        StopAllFades();

        this.concept.GetComponent<Text>().text = concept;
        this.conceptAmount.GetComponent<Text>().text = "x " + conceptAmount;
        this.koreanConcept.GetComponent<Text>().text = koreanConcept;

        panel.GetComponent<CanvasGroup>().alpha = 1;
        conceptPanel.GetComponent<CanvasGroup>().alpha = 0;
        text.GetComponent<CanvasGroup>().alpha = 0;
        shaped.GetComponent<CanvasGroup>().alpha = 0;

        fadeAll = StartCoroutine("FadeAll");
    }

    private IEnumerator FadeAll()
    {
        float alpha = 0;
        while (alpha < 1)
        {
            alpha += speed;
            text.GetComponent<CanvasGroup>().alpha = alpha;
            yield return null;
        }

        yield return new WaitForSeconds(conceptTime);

        alpha = 1;
        while(alpha > 0)
        {
            alpha -= speed;
            text.GetComponent<CanvasGroup>().alpha = alpha;
            conceptPanel.GetComponent<CanvasGroup>().alpha += speed;
            yield return null;
        }

        yield return new WaitForSeconds(textTime);

        alpha = 1;
        while (alpha > 0)
        {
            alpha -= speed;
            panel.GetComponent<CanvasGroup>().alpha = alpha;
            yield return null;
        }

    }

    public void ShowShapingDone()
    {
        StopAllFades();

        panel.GetComponent<CanvasGroup>().alpha = 0;

        shaped.GetComponent<Text>().text = concept.GetComponent<Text>().text + " shaped";
        shaped.GetComponent<CanvasGroup>().alpha = 1;

        conceptPanel.GetComponent<CanvasGroup>().alpha = 0;
        text.GetComponent<CanvasGroup>().alpha = 0;

        fadeShaping = StartCoroutine("FadeShaping");
    }

    public void HideShapingDone()
    {
        StopAllFades();

        panel.GetComponent<CanvasGroup>().alpha = 1;

        shaped.GetComponent<Text>().text = concept.GetComponent<Text>().text + " shaped";
        shaped.GetComponent<CanvasGroup>().alpha = 1;

        conceptPanel.GetComponent<CanvasGroup>().alpha = 0;
        text.GetComponent<CanvasGroup>().alpha = 0;

        hideShaping = StartCoroutine("FadeOutShaping");
    }

    private IEnumerator FadeShaping()
    {
        float alpha = 0;
        while (alpha < 1)
        {
            alpha += speed;
            panel.GetComponent<CanvasGroup>().alpha = alpha;
            yield return null;
        }

        //yield return new WaitForSeconds(shapingTime);

        //alpha = 1;
        //while (alpha > 0)
        //{
        //    alpha -= speed;
        //    panel.GetComponent<CanvasGroup>().alpha = alpha;
        //    yield return null;
        //}
    }

    public IEnumerator FadeOutShaping()
    {
        float alpha = 1;
        while (alpha > 0)
        {
            alpha -= speed;
            panel.GetComponent<CanvasGroup>().alpha = alpha;
            yield return null;
        }

    }

    public void StopAllFades()
    {
        if (fadeAll != null)
        {
            StopCoroutine(fadeAll);
            fadeAll = null;
        }

        if (fadeShaping != null)
        {
            StopCoroutine(fadeShaping);
            fadeShaping = null;
        }

        if(hideShaping != null)
        {
            StopCoroutine(hideShaping);
            hideShaping = null;
        }
    }
}
