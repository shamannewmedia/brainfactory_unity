﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour {

    public GameObject one;
    public GameObject two;

    public float speed = 0.01f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        one.transform.Rotate(Vector3.up * Time.deltaTime * speed);

        two.transform.Rotate(Vector3.down * Time.deltaTime * speed);
    }
}
