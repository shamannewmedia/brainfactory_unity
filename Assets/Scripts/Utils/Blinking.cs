﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Blinking : MonoBehaviour {

    private bool blinking = false;
    public float interval = 1.0f;

    private float time = 0.0f;

    public Indicators indicator;

    public bool isRecording = false;

    void Start()
    {
        blinking = false;
        gameObject.GetComponent<Text>().enabled = false;
    }

	// Update is called once per frame
	void Update () {
        if (!blinking || !isRecording)
            return;

        if (!indicator.showIndicators)
        {
            if (gameObject.GetComponent<Text>().enabled)
                gameObject.GetComponent<Text>().enabled = false;

            return;
        }

        time += Time.deltaTime;
        if (time < interval)
            return;

        time = 0;
        gameObject.GetComponent<Text>().enabled = !gameObject.GetComponent<Text>().enabled;
    }

    public void ShowBlinking()
    {
        blinking = true;
    }

    public void HideBlinking()
    {
        blinking = false;
        gameObject.GetComponent<Text>().enabled = false;
    }
}
