﻿using UnityEngine;
using System.Collections;

public class ShowStuff : MonoBehaviour {

    float deltaTime = 0.0f;

    private bool showFPS;

    void Start()
    {
        Cursor.visible = false;
        showFPS = false;
    }

    void Update()
    {
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;

        if (Input.GetKeyDown(KeyCode.F))
            showFPS = !showFPS;

        if (Input.GetKeyDown(KeyCode.M))
            Cursor.visible = !Cursor.visible;

    }

    void OnGUI()
    {
        if (!showFPS)
            return;

        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(0, 0, w, h * 2 / 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h * 2 / 100;
        style.normal.textColor = Color.white;
        float msec = deltaTime * 1000.0f;
        float fps = 1.0f / deltaTime;
        string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
        GUI.Label(rect, text, style);

    }
}
