﻿using UnityEngine;
using System.Collections.Generic;

public static class Utils {

    public static Color hexToColor(string hex)
    {
        hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
        hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
        byte a = 255;//assume fully visible unless specified in hex
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        //Only use alpha if the string has enough characters
        if (hex.Length == 8)
        {
            a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);
        }
        return new Color32(r, g, b, a);
    }

    public static float[] PutInArray(float[] arr, float val, int pos)
    {
        float aux;
        for (int i = pos; i >= 0; i--)
        {
            aux = arr[i];
            arr[i] = val;
            val = aux;
        }

        return arr;
    }

    public static string[] PutInArray(string[] arr, string val, int pos)
    {
        string aux;
        for (int i = pos; i >= 0; i--)
        {
            aux = arr[i];
            arr[i] = val;
            val = aux;
        }

        return arr;
        
    }
   
}
