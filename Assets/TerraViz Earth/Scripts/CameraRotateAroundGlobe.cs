﻿//Simple camera rotate script - trimmed down version of CameraRotateAroundGlobe in TerraViz
//Created by Julien Lynge @ Fragile Earth Studios

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraRotateAroundGlobe : MonoBehaviour
{
    public float altMiles = 800f;
    public float minAltitude = 100f;
    public float maxAltitude = 15000f;
	public float minAltitudeInSphere = 2100f;

    public float lat = 30f, lon = 180f;

    public float rotateSpeed = 100f;
	public bool zoomOut = true;

    void Start()
    {
        //altMiles = maxAltitude / 2f;
		altMiles = minAltitudeInSphere;
        applyPosInfoToTransform();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #region Input events

    void Update()
    {
			if(lon > 0f)
			{
				lon -= 2.0f * Time.deltaTime;
				if(altMiles >= minAltitudeInSphere && altMiles <= maxAltitude /2f && zoomOut == true)
				{
					altMiles += 50 * Time.deltaTime;
				}
				if(altMiles > maxAltitude / 2f)
				{
					zoomOut = false;
					altMiles -= 50 * Time.deltaTime;
				}
				if(altMiles >= minAltitudeInSphere && altMiles <= maxAltitude /2f && zoomOut == false){
					altMiles -= 50 * Time.deltaTime;
				}
				if(altMiles < minAltitudeInSphere)
				{
					zoomOut = true;
					altMiles += 50 * Time.deltaTime;
				}

				applyPosInfoToTransform();
			}
			if(lon <= 0)
			{
				lon += 360f;
			}

			
            //Move camera
            /*if (Input.GetMouseButton(0) || Input.GetAxis("Mouse ScrollWheel") != 0) //user is leftclick dragging - move camera along lat/lon
            {
                if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
                {
                    Vector2 posChange = new Vector2(-Input.GetAxis("Mouse X") * rotateSpeed * altMiles / maxAltitude, -Input.GetAxis("Mouse Y") * rotateSpeed * altMiles / maxAltitude);
                    lon += posChange.x;
                    lat += posChange.y;
                }
                
                if (Input.GetAxis("Mouse ScrollWheel") != 0)
                {
                    float smoothedTime = Mathf.Sqrt(Time.deltaTime / 0.02f);
                    altMiles *= 1f - Mathf.Clamp(Input.GetAxis("Mouse ScrollWheel") * smoothedTime * 1f, -.8f, .4f);
                altMiles = Mathf.Clamp(altMiles, minAltitude, maxAltitude);
                }

                lat = Mathf.Clamp(lat, -90f, 90f);

                applyPosInfoToTransform();
            }*/
    }

    protected void applyPosInfoToTransform()
    {
        Quaternion rotation = Quaternion.Euler(lat, -lon, 0);
        Vector3 position = -(Quaternion.Euler(lat, -lon, 0) * Vector3.forward * (altMiles * 1000f / 3954.44494f + 1500f));

        transform.rotation = rotation;
        transform.position = position;
    }

    #endregion
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
